package com.scmaster.css1.livefeed;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Optional;
import java.util.TimeZone;

import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.scmaster.css1.common.NumericLocal;
import com.scmaster.css1.dao.LivefeedDAO;
import com.scmaster.css1.dao.MemberDAO;
import com.scmaster.css1.vo.Livefeed;
import com.scmaster.css1.vo.Member;
import com.scmaster.css1.vo.UploadedFile;

@RequestMapping(value="livefeed")
@Controller
public class LivefeedController {
	
	@Autowired
	LivefeedDAO dao;
	
	@Autowired
	MemberDAO mdao;	
	
	private static final Logger logger = LoggerFactory.getLogger(LivefeedController.class);
	
	/*라이브피드로 이동*/
	@RequestMapping(value="livefeedForm", method=RequestMethod.GET)
	public String livefeedForm(){
		logger.info("move to livefeed page start");
		
		
		logger.info("move to livefeed page end");
		return "livefeed/livefeed";
	}
	
	/*모든 라이브피드 리스트 불러오기*/
	@RequestMapping(value="getlivefeed", method=RequestMethod.GET)
	public @ResponseBody ArrayList<Livefeed> livefeedList(
			Optional<Integer> lno) {
		ArrayList<Livefeed> livefeed = null;
		logger.info("get livefeed list start");
		System.out.println(lno);
		livefeed = dao.selectLivefeed(lno.isPresent()?lno.get():0);
		System.out.println(lno);
		System.out.println(livefeed);		
		logger.info("get livefeed list end");
		return livefeed;
	}
	
	/*라이브피드 입력하기*/
	@RequestMapping(value="insertlivefeed", method=RequestMethod.POST)
	public @ResponseBody int insertLivefeed(Livefeed livefeed,HttpSession session) {
		int result =0;
		logger.info("insert livefeed start");
		if(session.getAttribute("loginUser")!=null) {
			logger.info("livefeed: "+livefeed.toString());
			livefeed.setUsernum((int)session.getAttribute("loginNum"));
			livefeed.setUsername((String)session.getAttribute("loginId"));
			result = dao.insertLivefeed(livefeed);
		}
		logger.info("insert livefeed end");
		
		return result;
		
/*		private int usernum; 유저번호 n
		private String username;	아이디n
		private int lno; 지자체번호
		private String lname;	지자체 이름
		private int cno; 기초자치단체번호
		private String clname;	기초자치단체이름
		private String content; livefeed 내용
		private String posted;	작성날자 n
		private int attachtype;	첨부파일 양식
		private String attach; 첨부파일명*/
	}
	
	/*라이브피드 삭제하기*/
	@RequestMapping(value="deletelivefeed", method=RequestMethod.GET)
	public int deleteLivefeed(Livefeed livefeed, HttpSession session) {
		int result =0;
		logger.info("delete livefeed start");
		
		
		
		
		logger.info("delete livefeed end");
		return result;
	}
	
	/*신고하기*/
	@RequestMapping(value="report", method=RequestMethod.GET)
	public @ResponseBody ResponseEntity<HashMap<String,Object>> reportUser( int usernum, HttpSession session) {
		int result =0;
		HashMap<String, Object> rmap = new HashMap<String, Object>();
		HashMap<String, Object> resultmap = new HashMap<String, Object>();
		
		System.out.println("report user start");
		int sender1 = (int)session.getAttribute("loginNum");	
		String sender = String.valueOf(sender1); 	
		
		rmap.put("target", usernum);
		rmap.put("sender", sender);
		
		result =mdao.reportUser(rmap);
		System.out.println(result);
		System.out.println("report user end");
		
		resultmap.put("result", result);
		return new ResponseEntity<HashMap<String,Object>>(resultmap, result > 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
	}
	
	@ResponseBody
	@RequestMapping(value="listken", method=RequestMethod.GET)
	public ArrayList<HashMap<String, Object>> listKen(Model model){
		ArrayList<HashMap<String, Object>> kenlist = null;
		kenlist =dao.listTodohuken();
		//lno and lname 만 가져와서 보냄		
		System.out.println(kenlist);				
		return kenlist;
	}
	
	/*자동완성용 도도부현리스트*/
	@ResponseBody
	@RequestMapping(value="listkenAc", method=RequestMethod.GET)
	public ArrayList<HashMap<String,Object>> listKenAc(Model model){
		ArrayList<HashMap<String,Object>> kenlist = null;
		kenlist =dao.listken();
		//lno and lname 만 가져와서 보냄		
		System.out.println(kenlist);			
		return kenlist;
	}
	
	@ResponseBody
	@RequestMapping(value="listshiku", method=RequestMethod.GET)
	public ArrayList<HashMap<String, Object>> listShiku(int value){
		ArrayList<HashMap<String, Object>> shikulist = null;
		
		shikulist = dao.listShiku(value);
		if(shikulist != null){
			HashMap<String,Object> noSelect = new HashMap<>();
			noSelect.put("name", "(無し)");
			noSelect.put("value", 0);
			noSelect.put("selected", true);
			shikulist.add(0,noSelect);
		}
		System.out.println(shikulist);
		return shikulist;
	}
	
	//String targetPath = "/img/";
	String targetPath = "/usr/share/nginx/html/tomcat8/kiokie_files/";
	@ResponseBody
	@RequestMapping(value="uploadProcess", method={RequestMethod.POST, RequestMethod.GET})
	public ResponseEntity<HashMap<String, Object>> uploadProcess(HttpServletRequest request){
		HashMap<String,Object> hmResult = new HashMap<>();
		ArrayList<UploadedFile> alFiles = new ArrayList<>();
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
	    Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
	    MultipartFile multipartFile = null;
	    while(iterator.hasNext()){
	    	UploadedFile file = new UploadedFile();
	        multipartFile = multipartHttpServletRequest.getFile(iterator.next());
	        if(multipartFile.isEmpty() == false){
	            logger.info("------------- file start -------------");
	            logger.info("name : "+multipartFile.getName());
	            logger.info("filename : "+multipartFile.getOriginalFilename());
	            logger.info("size : "+multipartFile.getSize());
	            
	            String ext = null;
	            logger.info("mime: "+multipartFile.getContentType());
	            switch(multipartFile.getContentType()){
		            case "image/jpeg":
		            	ext = ".jpg";
		            	break;
		            case "image/png":
		            	ext = ".png";
		            	break;
		            case "image/gif":
		            	ext = ".gif";
		            	break;
		            case "video/mp4":
		            	ext = ".mp4";
		            	break;
		            case "video/avi":
		            	ext = ".avi";
		            	break;
	            }
	        
	            int ext_period = multipartFile.getOriginalFilename().indexOf('.');
	            if(ext_period <= 0){
	            	//확장자가 없는 파일
	            	logger.info("확장자가 없음");
	            	continue;
	            }
	            String name = multipartFile.getOriginalFilename().substring(0,ext_period);
	            
	            file.setName(multipartFile.getOriginalFilename());
	            file.setSize(multipartFile.getSize());
	            //
	            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("utc"));
	            String filename = String.format("%d%d%s", System.currentTimeMillis(),name.length(),ext);
	            //FileOutputStream fos = new FileOutputStream(targetPath + )
	            String targetFilename = targetPath + filename;
	            File f = new File(targetFilename);
	            try {
					multipartFile.transferTo(f);
					
					//결과값 출력을 위한 부분
		            file.setName(multipartFile.getOriginalFilename());
		            String myurl = request.getContextPath()+"/livefeed/files/";
		            file.setUrl(filename);
		            file.setThumbnailUrl(filename);
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					file.setError(e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					file.setError(e.toString());
					e.printStackTrace();
				}            
	            
	            logger.info("-------------- file end --------------\n");
	        }
	        alFiles.add(file);
	    }
	    if(alFiles == null || alFiles.isEmpty()){
	    	hmResult.put("error", "no files accepted");
	    	logger.info("Upload: alFiles empty.");
	    	return new ResponseEntity<HashMap<String,Object>>(hmResult,HttpStatus.BAD_REQUEST);
	    }
	    hmResult.put("files", alFiles);
	    return new ResponseEntity<HashMap<String,Object>>(hmResult, HttpStatus.OK);	
	}
	
	@ResponseBody
	@RequestMapping(value="files/{imgfile:.+}")
	public ResponseEntity<byte[]> getAttachedImg(@PathVariable String imgfile) {
		FileInputStream in = null;
		try{
			FileInputStream fis = new FileInputStream(targetPath + imgfile);
			
			return new ResponseEntity<byte[]>(IOUtils.toByteArray(fis), HttpStatus.OK);			
		}catch(FileNotFoundException e){
			logger.info("getAttachedImg: file not found - "+imgfile);
		}catch(Exception e){
			e.printStackTrace();
		}
		return new ResponseEntity<byte[]>(HttpStatus.NOT_FOUND);
	}
}
