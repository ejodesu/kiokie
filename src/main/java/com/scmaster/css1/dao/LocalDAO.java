package com.scmaster.css1.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import com.scmaster.css1.vo.POI;
import com.scmaster.css1.vo.Todoufuken;
import com.scmaster.css1.vo.Weather;

@Repository
public class LocalDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(LocalDAO.class);

	@Autowired
	SqlSession sqlsession;
	
	public Todoufuken seacrhLocal(int lno){
		logger.info("위치정보 불러오기 시작");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		Todoufuken local = new Todoufuken();
		try {
			local = mapper.seacrhLocal(lno);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("위치정보 불러오기 종료");
		return local;
	}
	
	public Todoufuken searchLocalByName(String lname){
		logger.info("위치정보 불러오기 시작");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		Todoufuken local = new Todoufuken();
		try {
			local = mapper.searchLocalByName(lname);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("위치정보 불러오기 종료");
		return local;
	}
	
	public ArrayList<POI> searchPOIByPoint(HashMap<String, Object> loc){
		logger.info("POI Point 검색 시작");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		ArrayList<POI> hosList = new ArrayList<POI>();
		try {
			hosList = mapper.searchPOIByPoint(loc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("POI Point 검색 종료");
		return hosList;
	}
	
	public ArrayList<POI> searchPOIByRect(HashMap<String, Object> loc){
		logger.info("POI Rect 검색 시작");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		ArrayList<POI> hosList = new ArrayList<POI>();
		try {
			hosList = mapper.searchPOIByRect(loc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("POI Rect 검색 종료");
		return hosList;
	}
	
	/*뉴스 읽어오기*/	
	public ArrayList<HashMap<String,Object>> readNews(String title){
		logger.info("뉴스 불러오기 시작");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		ArrayList<HashMap<String,Object>> newsList = new ArrayList<HashMap<String,Object>>();
		try {
			newsList = mapper.readNews(title);
		}catch(Exception e) {
			e.printStackTrace();			
		}		
		logger.info("뉴스 불러오기 종료");
		return newsList;
	}
	
	public String selectWeatherScore(String title){
		logger.info("점수 불러오기 시작");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		String result = null;
		try {
			result = mapper.selectWeatherScore(title);
		}catch(Exception e) {
			e.printStackTrace();			
		}		
		logger.info("점수 불러오기 종료");
		return result;
	}
	
	/*날씨 정보 불러오기*/
	public ArrayList<Weather> searchWeather(){
		logger.info("날씨 정보 불러오기 시작: DAO");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		ArrayList<Weather> weatherList = new ArrayList<Weather>();
		try {
			weatherList = mapper.searchWeather();
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		logger.info("날씨 정보 불러오기 종료: DAO");
		return weatherList;
		
		
	}
	
	/*알림 불러오기*/
	public ArrayList<HashMap<String,Object>> selectAlert(){
		logger.info("알림 불러오기 시작: DAO");
		LocalMapper mapper = sqlsession.getMapper(LocalMapper.class);
		ArrayList<HashMap<String,Object>> result = new ArrayList<>();
		try {
			result = mapper.selectAlert();
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		logger.info("알림 불러오기 종료: DAO");
		return result;
		
		
	}
	
}
