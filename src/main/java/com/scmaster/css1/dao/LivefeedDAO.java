package com.scmaster.css1.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.scmaster.css1.vo.Livefeed;

@Repository
public class LivefeedDAO {
	
	@Autowired
	SqlSession sqlsession;
	
	private static final Logger logger = LoggerFactory.getLogger(LivefeedDAO.class);
	
	/*livefeed 불러오기*/
	public ArrayList<Livefeed> selectLivefeed(int lno) {
		ArrayList<Livefeed> list=null;
		LivefeedMapper mapper = sqlsession.getMapper(LivefeedMapper.class);
		try {
			list = mapper.selectLivefeed(lno);
			
		}catch(Exception e) {
			e.printStackTrace();
		}		
		
		return list;
	}
	
	/*livefeed 입력*/
	public int insertLivefeed(Livefeed livefeed) {
		int result =0;
		LivefeedMapper mapper = sqlsession.getMapper(LivefeedMapper.class);
		try {
			result = mapper.insertLivefeed(livefeed);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/*livefeed 수정*/
	public Livefeed updateLivefeed() {
		Livefeed livefeed=null;
		LivefeedMapper mapper = sqlsession.getMapper(LivefeedMapper.class);
		try {
			livefeed = mapper.updateLivefeed();
		}catch(Exception e) {
			e.printStackTrace();
		}		
		return livefeed;
	}
	
	/*livefeed 삭제*/
	public int deleteLivefeed(int no, int usernum) {
		int result =0;
		LivefeedMapper mapper = sqlsession.getMapper(LivefeedMapper.class);
		try {
			result = mapper.deleteLivefeed(no, usernum);
		}catch(Exception e) {
			e.printStackTrace();
		}		
		return result;
	}
	
	/*시-구 리스트 제공*/
	public ArrayList<HashMap<String,Object>> listShiku(int ken){
		ArrayList<HashMap<String,Object>> result =null;
		MemberMapper mapper =sqlsession.getMapper(MemberMapper.class);
		try{
			result = mapper.listShiku(ken);
		}catch(Exception e){
			e.printStackTrace();
		}		
		return result;
		
	}
	
	/*광역자치단체 리스트 제공*/
	public ArrayList<HashMap<String,Object>> listTodohuken(){
		ArrayList<HashMap<String,Object>> result =null;
		MemberMapper mapper =sqlsession.getMapper(MemberMapper.class);
		try{
			result = mapper.listTodohuken();
		}catch(Exception e){
			e.printStackTrace();
		}		
		return result;
		
	}
	
	/*도도부현 자동완성용*/
	public ArrayList<HashMap<String,Object>> listken(){
		ArrayList<HashMap<String,Object>> result =null;
		MemberMapper mapper =sqlsession.getMapper(MemberMapper.class);
		try{
			result = mapper.listken();
		}catch(Exception e){
			e.printStackTrace();
		}		
		return result;
	}

}
