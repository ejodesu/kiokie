package com.scmaster.css1.dao;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scmaster.css1.vo.Member;

@Repository
public class MemberDAO {
	
	@Autowired
	SqlSession sqlsession;
	
	/**
	 * 회원 가입 처리
	 * @param member 사용자가 입력한 가입 정보
	 */
	public int insertMember(Member member){
		int result =0;		
		MemberMapper mapper= sqlsession.getMapper(MemberMapper.class);		
		try{
			result = mapper.insertMember(member);
		}catch(Exception e){
			e.printStackTrace();
		}				
		return result;		
	}
	
	public int editMember(Member member){
		int result =0;
		MemberMapper mapper = sqlsession.getMapper(MemberMapper.class);
		try {
			result= mapper.editMember(member);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	/**
	 * ID(username)로 회원정보 검색
	 * @param id 검색할 아이디
	 * @return 검색된 회원정보. 없으면 null.
	 */
	
	public Member selectMember(Member member){		
		MemberMapper mapper = sqlsession.getMapper(MemberMapper.class);
		try{			
			member = mapper.selectMember(member);		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return member;
	}
	
	
	/*id(username)으로 회원을 검색하여 아이디가 중복인지 아닌지를 확인하기 위함*/
	
	public Member searchMember(String username){
		Member member =null;
		MemberMapper mapper =sqlsession.getMapper(MemberMapper.class);
		try{
			member = mapper.searchMember(username);
		}catch(Exception e){
			e.printStackTrace();
		}		
		return member;
		
	}
	
	/*시-구 리스트 제공*/
	public ArrayList<HashMap<String,Object>> listShiku(int ken){
		ArrayList<HashMap<String,Object>> result =null;
		MemberMapper mapper =sqlsession.getMapper(MemberMapper.class);
		try{
			result = mapper.listShiku(ken);
		}catch(Exception e){
			e.printStackTrace();
		}		
		return result;
		
	}
	
	/*광역자치단체 리스트 제공*/
	public ArrayList<HashMap<String,Object>> listTodohuken(){
		ArrayList<HashMap<String,Object>> result =null;
		MemberMapper mapper =sqlsession.getMapper(MemberMapper.class);
		try{
			result = mapper.listTodohuken();
		}catch(Exception e){
			e.printStackTrace();
		}		
		return result;
		
	}
	
	/*도도부현 자동완성용*/
	public ArrayList<HashMap<String,Object>> listken(){
		ArrayList<HashMap<String,Object>> result =null;
		MemberMapper mapper =sqlsession.getMapper(MemberMapper.class);
		try{
			result = mapper.listken();
		}catch(Exception e){
			e.printStackTrace();
		}		
		return result;
	}
	
	public Member selectMemberbynum(int usernum) {
		Member member =null;
		MemberMapper mapper = sqlsession.getMapper(MemberMapper.class);
		try {
			member = mapper.selectMemberbynum(usernum);
		}catch(Exception e) {
			e.printStackTrace();
		}		
		
		return member;
	}
	
	/*회원 조지기 기능*/
	public int reportUser(HashMap<String, Object> rmap) {
		int result = 0;
		MemberMapper mapper = sqlsession.getMapper(MemberMapper.class);
		try {			
			result = mapper.reportUser(rmap);
		}catch(Exception e) {			
			e.printStackTrace();
		}
		
		return result;
	}
}
