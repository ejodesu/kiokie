package com.scmaster.css1.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.scmaster.css1.vo.Member;

public interface MemberMapper {
	
	/*로그인을 위한 멤버확인*/
	public Member selectMember(Member member);
	
	/*아이디 중복확인*/
	public Member searchMember(String username);
	
	/*회원가입*/
	public int insertMember(Member member);

	/*회원정보수정*/
	public int editMember(Member member);
	
	/*도도부현 리스트*/
	public ArrayList<HashMap<String,Object>> listTodohuken();
	
	/*자동완성용 도도부현 리스트*/
	public ArrayList<HashMap<String,Object>> listken();
	
	/*기초자치단체 리스트*/
	public ArrayList<HashMap<String,Object>> listShiku(int lno);
	
	/*usernum으로 user 찾아오기*/
	public Member selectMemberbynum(int usernum);
	
	/*신고하기*/	
	public int reportUser(HashMap<String,Object> rmap);
	
	
	
	
}
