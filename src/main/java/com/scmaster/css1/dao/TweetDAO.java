package com.scmaster.css1.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.scmaster.css1.vo.Tweet;

@Repository
public class TweetDAO {
	
	@Autowired
	SqlSession session;
	
	public ArrayList<Tweet> selectTwitter(){
		ArrayList<Tweet> tweet =null;
		TweetMapper mapper = session.getMapper(TweetMapper.class);
		try {
			tweet = mapper.selectTwitter();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return tweet;
	}

}
