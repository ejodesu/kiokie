package com.scmaster.css1.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.scmaster.css1.vo.Livefeed;

public interface LivefeedMapper {
	
	/*livefeed를 모두 불러오기*/	
	public ArrayList<Livefeed> selectLivefeed(int lno);
	
	/*livefeed 입력*/
	public int insertLivefeed(Livefeed livefeed);
	
	/*livefeed 수정*/
	public Livefeed updateLivefeed();
	
	/*livefeed 삭제*/
	public int deleteLivefeed(int no, int usernum);
	
	/*도도부현 리스트*/
	public ArrayList<HashMap<String,Object>> listTodohuken();
	
	/*자동완성용 도도부현 리스트*/
	public ArrayList<HashMap<String,Object>> listken();
	
	/*기초자치단체 리스트*/
	public ArrayList<HashMap<String,Object>> listShiku(int lno);
	
}
