package com.scmaster.css1.dao;

import java.util.ArrayList;
import java.util.HashMap;

import com.scmaster.css1.vo.POI;
import com.scmaster.css1.vo.Todoufuken;
import com.scmaster.css1.vo.Weather;

public interface LocalMapper {

	public Todoufuken seacrhLocal(int lno);
	
	public Todoufuken searchLocalByName(String lname);
	
	public ArrayList<POI> searchPOIByPoint(HashMap<String, Object> loc);	
	public ArrayList<POI> searchPOIByRect(HashMap<String, Object> loc);	
	
	/*날씨 정보 불러오기*/
	public ArrayList<Weather> searchWeather();
	
	/*뉴스 불러오기*/
	public ArrayList<HashMap<String,Object>> readNews(String title);
	
	/*점수 불러오기*/
	public String selectWeatherScore(String title);
	
	/*알림 불러오기*/
	public ArrayList<HashMap<String,Object>> selectAlert();
}
