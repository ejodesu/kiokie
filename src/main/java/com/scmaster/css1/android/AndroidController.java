package com.scmaster.css1.android;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.scmaster.css1.common.TempKey;
import com.scmaster.css1.dao.LivefeedDAO;
import com.scmaster.css1.dao.LocalDAO;
import com.scmaster.css1.dao.MemberDAO;
import com.scmaster.css1.member.MemberController;
import com.scmaster.css1.vo.Livefeed;
import com.scmaster.css1.vo.Member;
import com.scmaster.css1.vo.Todoufuken;
import com.scmaster.css1.vo.UploadedFile;

@Controller
@RequestMapping(value="app")
public class AndroidController {
	private static final Logger logger = LoggerFactory.getLogger(AndroidController.class);
	
	HashMap<String,Member> mapMember = new HashMap<>();
	
	@Autowired
	MemberDAO dao;
	@Autowired
	LocalDAO ldao;
	@Autowired
	LivefeedDAO lvdao;
	
	@RequestMapping(value="login", method=RequestMethod.GET)
	public @ResponseBody String login(Member memberEx){
		logger.info("회원정보폼 이동 시작");
		Member member = dao.selectMember(memberEx);
		//HashMap<String,Object> result = new HashMap<>();
		String sresult = null;
		if(member != null){
			/*result.put("name", member.getUsername());
			result.put("success", "ok");
			TempKey tk = new TempKey();
			String newkey = tk.getKey(7, false);
			while(mapMember.containsKey(newkey)){
				newkey = tk.getKey(7, false);
			}
			result.put("token", newkey);
			mapMember.put(newkey, member);*/
			sresult = "ok";
		}else{
			//result.put("success", "error");
			sresult = "fail";
		}
		
		System.out.println(member.getEmail());
		logger.info("회원정보폼 이동 종료");
		
		return sresult;
	}
	
	@RequestMapping(value="start", method=RequestMethod.GET)
	public ResponseEntity<String> sessionStart(Member memberEx, HttpSession session){
		logger.info("android astart with ");
		Member member = dao.selectMember(memberEx);
		if(member!=null){
			//토큰이 유효함
			System.out.println(member.getLno());
			Todoufuken local = ldao.seacrhLocal(member.getLno());
			System.out.println(local.getLno());
			session.setAttribute("loginLat", member.getLatitude());
			session.setAttribute("loginLng", member.getLongitude());
			session.setAttribute("loginUser", member.getUsername());
			session.setAttribute("loginNum", member.getUsernum());
		}else{
			//토큰을 서버 재시작 등으로 활용할 수 없을 때 400 반환
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST); 
		}
		return new ResponseEntity<String>("location:/",HttpStatus.OK);
	}
	/*android livefeed post작성과 livefeed file upload controller*/
	String targetPath = "/img/";
	@RequestMapping(value="write", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> writePost(Livefeed livefeed, MultipartHttpServletRequest request){
		logger.info("android file upload start");
		ResponseEntity<String> entity = null;
		String token = (String)request.getAttribute("token");
		if(token == null){
			return new ResponseEntity<String>("no token",HttpStatus.BAD_REQUEST);
		}
		String[] sliced_token = token.split("|");
		if(sliced_token == null){
			return new ResponseEntity<String>("wrong token: "+token,HttpStatus.BAD_REQUEST);
		}
		Member memberEx = new Member(sliced_token[0],sliced_token[1]);
		Member member = dao.selectMember(memberEx);
		//Livefeed livefeed = (Livefeed)request.getAttribute("livefeed");
		if(livefeed == null){
			return new ResponseEntity<String>("no livefeed",HttpStatus.BAD_REQUEST);
		}
		if(member!=null){
			/*로그인*/			
			Todoufuken local = ldao.seacrhLocal(member.getLno());			
			/******************************************************************/
			HashMap<String,Object> hmResult = new HashMap<>();
			ArrayList<UploadedFile> alFiles = new ArrayList<>();
			MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
		    Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		    MultipartFile multipartFile = null;
		    while(iterator.hasNext()){
		    	UploadedFile file1 = new UploadedFile();
		        multipartFile = multipartHttpServletRequest.getFile(iterator.next());
		        if(multipartFile.isEmpty() == false){
		            logger.info("------------- file start -------------");
		            logger.info("name : "+multipartFile.getName());
		            logger.info("filename : "+multipartFile.getOriginalFilename());
		            logger.info("size : "+multipartFile.getSize());
		            logger.info("typ : "+multipartFile.getContentType());
		            
		            String ext = null;
		            switch(multipartFile.getContentType()){
			            case "image/jpeg":
			            	ext = ".jpg";
			            	break;
			            case "image/png":
			            	ext = ".png";
			            	break;
			            case "image/gif":
			            	ext = ".gif";
			            	break;
			            case "video/mp4":
			            	ext = ".mp4";
			            	break;
			            case "video/avi":
			            	ext = ".avi";
			            	break;
		            }
		        
		            int ext_period = multipartFile.getOriginalFilename().indexOf('.');
		            if(ext_period <= 0){
		            	//확장자가 없는 파일
		            	logger.info("확장자가 없음");
		            	continue;
		            }
		            String name = multipartFile.getOriginalFilename().substring(0,ext_period);
		            
		            file1.setName(multipartFile.getOriginalFilename());
		            file1.setSize(multipartFile.getSize());
		            //
		            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("utc"));
		            String filename = String.format("%d%d%s", System.currentTimeMillis(),name.length(),ext);
		            //FileOutputStream fos = new FileOutputStream(targetPath + )
		            String targetFilename = targetPath + filename;
		            File f = new File(targetFilename);
		            try {
						multipartFile.transferTo(f);
						
						//결과값 출력을 위한 부분
			            file1.setName(multipartFile.getOriginalFilename());
			            String myurl = request.getContextPath()+"/livefeed/files/";
			            file1.setUrl(filename);
			            file1.setThumbnailUrl(filename);
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						file1.setError(e.toString());
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						file1.setError(e.toString());
						e.printStackTrace();
					}            
		            
		            logger.info("-------------- file end --------------\n");
		        }
		        alFiles.add(file1);
		    }
		    hmResult.put("files", alFiles);			
			/************************************************************************/
		    logger.info("android file upload end");
		    
		    logger.info("livefeed from android: "+lvdao.insertLivefeed(livefeed));		
		}else{
			/*member 가 null 일 경우...는 로그인 실패*/
			return new ResponseEntity("login fail",HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity("ok",HttpStatus.OK);
	}/*write post*/
}
