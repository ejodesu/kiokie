package com.scmaster.css1.vo;

/*라이브피드*/
public class Livefeed {
	
	
	private int no;	/*라이브피드번호*/
	private int usernum; /*유저번호*/
	private String username;	/*아이디*/
	private int lno; /*지자체번호*/
	private String lname;	/*지자체 이름*/
	private int cno; /*기초자치단체번호*/
	private String clname;	/*기초자치단체이름*/
	private String content; /*livefeed 내용*/
	private String posted;	/*작성날자*/ 
	private int attachtype;	/*첨부파일 양식*/
	private String attach; /*첨부파일명*/
	private String color; /*도도부현 컬러*/
	private int trycount; /*글쓴이의 신고받은 횟수*/
	
	public Livefeed () {}	/*default constructor*/
	
	public Livefeed(int no, int usernum, String username, int lno, String lname, int cno, String clname, String content, 
			String posted, int attachtype, String attach, String color, int trycount) {
		
		this.no =no;
		this.usernum = usernum;
		this.username = username;
		this.lno = lno;
		this.lname = lname;
		this.cno = cno;
		this.clname = clname;
		this.content = content;
		this.posted = posted;
		this.attachtype = attachtype;
		this.attach = attach;
		this.color =color;
		this.trycount = trycount;
		
	}
		
	/*getter and setter*/
	
	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getUsernum() {
		return usernum;
	}

	public void setUsernum(int usernum) {
		this.usernum = usernum;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getLno() {
		return lno;
	}

	public void setLno(int lno) {
		this.lno = lno;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public String getClname() {
		return clname;
	}

	public void setClname(String clname) {
		this.clname = clname;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPosted() {
		return posted;
	}

	public void setPosted(String posted) {
		this.posted = posted;
	}

	public int getAttachtype() {
		return attachtype;
	}

	public void setAttachtype(int attachtype) {
		this.attachtype = attachtype;
	}

	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}

	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	

	public int getTrycount() {
		return trycount;
	}

	public void setTrycount(int trycount) {
		this.trycount = trycount;
	}
	
	
	/*toString*/	
	@Override
	public String toString() {
		return "Livefeed [no=" + no + ", usernum=" + usernum + ", username=" + username + ", lno=" + lno + ", lname="
				+ lname + ", cno=" + cno + ", clname=" + clname + ", content=" + content + ", posted=" + posted
				+ ", attachtype=" + attachtype + ", attach=" + attach + ", color=" + color + ", trycount=" + trycount
				+ "]";
	}

	
	
	
	
	
	
	
}
