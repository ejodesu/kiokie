package com.scmaster.css1.vo;

public class Tweet {
	private long no; //트윗번호
	private String keyword;	//검색한 키워드
	private int type;		//Bit 연산타입>사건 사고 재해 구분 변수
	private String content;		//트윗내용
	private String author;		//작성자
	private String avatar;
	private int lno; 	//도도부현
	private int cno;	//시 구
	private String posted;		//작성시각
	private String seen; 	//발견시각

	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Tweet() {}	//default constructor
	
	public Tweet(long id, String keyword, int type, String content, String author, String avatar,
			int lno, int cno, String posted) {
		this.no= id;
		this.keyword = keyword;
		this.type = type;
		this.content = content;
		this.author = author;
		this.avatar = avatar;
		this.lno = lno;
		this.cno= cno;
		this.posted = posted;
		
	}
	
	/*getter and setter*/

	public long getNo() {
		return no;
	}

	public void setNo(long no) {
		this.no = no;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getLno() {
		return lno;
	}

	public void setLno(int lno) {
		this.lno = lno;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public String getPosted() {
		return posted;
	}

	public void setPosted(String posted) {
		this.posted = posted;
	}

	public String getSeen() {
		return seen;
	}

	public void setSeen(String seen) {
		this.seen = seen;
	}
	
	/*toString*/

	@Override
	public String toString() {
		return "Tweet [no=" + no + ", keyword=" + keyword + ", type=" + type + ", content=" + content + ", author="
				+ author + ", avatar=" + avatar + ", lno=" + lno + ", cno=" + cno + ", posted=" + posted + ", seen="
				+ seen + "]";
	}
	
	
	
}
