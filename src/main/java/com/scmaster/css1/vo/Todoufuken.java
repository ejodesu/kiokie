package com.scmaster.css1.vo;

public class Todoufuken {
	private int lno;
	private String lname;
	private String lyomi;
	private String lsname;
	private double longitude;
	private double latitude;
	private String color;
	
	public Todoufuken() {
		super();
	}

	public Todoufuken(int lno, String lname, String lyomi, String lsname, double longitude, double latitude,
			String color) {
		super();
		this.lno = lno;
		this.lname = lname;
		this.lyomi = lyomi;
		this.lsname = lsname;
		this.longitude = longitude;
		this.latitude = latitude;
		this.color = color;
	}

	public int getLno() {
		return lno;
	}

	public void setLno(int lno) {
		this.lno = lno;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getLyomi() {
		return lyomi;
	}

	public void setLyomi(String lyomi) {
		this.lyomi = lyomi;
	}

	public String getLsname() {
		return lsname;
	}

	public void setLsname(String lsname) {
		this.lsname = lsname;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Todouhuken [lno=" + lno + ", lname=" + lname + ", lyomi=" + lyomi + ", lsname=" + lsname
				+ ", longitude=" + longitude + ", latitude=" + latitude + ", color=" + color + "]";
	}
	
}
