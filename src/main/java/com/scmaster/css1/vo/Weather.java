package com.scmaster.css1.vo;

public class Weather {
	private int weather;
	private int cno;
	private String weatherdesc;
	private float temperature;
	private float min;
	private float max;
	private int pm25;
	private float score;
	private String updated;
	private String alert;
	private String url;
	private double lat;
	private double lng;
	private String lsname;
	
	public Weather() {
		
	}
	public Weather(int weather, String weatherdesc, float temperature, float min, float max, float score,
			String updated, String alert, String url, int pm25) {
		this.weather = weather;
		this.weatherdesc = weatherdesc;
		this.temperature = temperature;
		this.min = min;
		this.max = max;
		this.score = score;
		this.updated = updated;
		this.alert = alert;
		this.url = url;
		this.pm25 = pm25;
	}
	
	
		
	@Override
	public String toString() {
		return "Weather [weather=" + weather + ", cno=" + cno + ", weatherdesc=" + weatherdesc + ", temperature="
				+ temperature + ", min=" + min + ", max=" + max + ", pm25=" + pm25 + ", score=" + score + ", updated="
				+ updated + ", alert=" + alert + ", url=" + url + "]";
	}
	
	
	
	public int getPm25() {
		return pm25;
	}
	public void setPm25(int pm25) {
		this.pm25 = pm25;
	}
	public int getWeather() {
		return weather;
	}
	public void setWeather(int weather) {
		this.weather = weather;
	}
	public String getWeatherdesc() {
		return weatherdesc;
	}
	public void setWeatherdesc(String weatherdesc) {
		this.weatherdesc = weatherdesc;
	}
	public float getTemperature() {
		return temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	public float getMin() {
		return min;
	}
	public void setMin(float min) {
		this.min = min;
	}
	public float getMax() {
		return max;
	}
	public void setMax(float max) {
		this.max = max;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public String getAlert() {
		return alert;
	}
	public void setAlert(String alert) {
		this.alert = alert;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public String getLsname() {
		return lsname;
	}
	public void setLsname(String lsname) {
		this.lsname = lsname;
	}
	
	
}
