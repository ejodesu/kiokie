package com.scmaster.css1.vo;

public class POI {
	private double lat;
	private double lng;
	private String bname;
	private String desc;
	private int disasterType;
	private int fscale;
	private int seatingcap;
	private String subjects;
	private double distance;
	
	public int getFscale() {
		return fscale;
	}
	public void setFscale(int fscale) {
		this.fscale = fscale;
	}
	public int getSeatingcap() {
		return seatingcap;
	}
	public void setSeatingcap(int seatingcap) {
		this.seatingcap = seatingcap;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public String getBname() {
		return bname;
	}
	public void setBname(String bname) {
		this.bname = bname;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getDisasterType() {
		return disasterType;
	}
	public void setDisasterType(int disasterType) {
		this.disasterType = disasterType;
	}
	public String getSubjects() {
		return subjects;
	}
	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	@Override
	public String toString() {
		return "POI [lat=" + lat + ", lng=" + lng + ", bname=" + bname + ", desc=" + desc + ", disasterType="
				+ disasterType + ", subjects=" + subjects + ", distance=" + distance + "]";
	}
	public POI(double lat, double lng,String bname, String desc, int disasterType, String subjects, double distance) {
		this.lat = lat;
		this.lng = lng;
		this.desc = desc;
		this.disasterType = disasterType;
		this.subjects = subjects;
		this.distance = distance;
		this.bname = bname;
	}
	public POI() {
		
	}
}
