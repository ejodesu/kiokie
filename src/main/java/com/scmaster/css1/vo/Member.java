package com.scmaster.css1.vo;

/*회원의 정보
 * 회원번호, 이메일, 아이디, 비밀번호, 성별, 
 * 생년월일, 위치값(경도),위치값(위도),
 *  가입실패횟수, 권한 */

public class Member {
	
	private int usernum;	/*회원번호*/
	private String email;	/*이메일 */
	private String username;	/*아이디*/
	private String pass;	/*비밀번호*/
	private int sex;		/*성별*/
	private String birth;	/*생년월일*/
	private int lno; /*도도부현 번호*/
	private int cno; /*기초자치단체 번호*/
	private double longitude; /*경도*/
	private double latitude;	 /*위도*/
	private int trycount;	/*실패횟수*/
	private String posted;	/*게시일*/
	private int permission;	/*권한*/
	
	public Member(){}	/*default constructor*/
	public Member(String username, String pass){
		this.username = username;
		this.pass = pass;
	}
	
	public Member(int usernum, String email, String username, String pass, int sex, String birth
			,double longitude, double latitude, int trycount, String posted, int permission){
		
		this.usernum = usernum;
		this.email = email;
		this.username = username;
		this.pass = pass;
		this.sex = sex;
		this.birth = birth;
		this.longitude =longitude;
		this.latitude = latitude;
		this.trycount = trycount;
		this.posted = posted;
		this.permission = permission;
		
		
		/*getter and setter*/	
		
	}

	public int getUsernum() {
		return usernum;
	}

	public void setUsernum(int usernum) {
		this.usernum = usernum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getBirth() {
		return birth;
	}
	
	public void setBirth(String birth) {
		this.birth = birth;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public int getTrycount() {
		return trycount;
	}

	public void setTrycount(int trycount) {
		this.trycount = trycount;
	}

	public String getPosted() {
		return posted;
	}

	public void setPosted(String posted) {
		this.posted = posted;
	}

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}
	
	//toString

	public int getLno() {
		return lno;
	}

	public void setLno(int lno) {
		this.lno = lno;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	@Override
	public String toString() {
		return "Member [usernum=" + usernum + ", email=" + email + ", username=" + username + ", pass=" + pass
				+ ", sex=" + sex + ", birth=" + birth + ", longitude=" + longitude + ", latitude=" + latitude
				+ ", trycount=" + trycount + ", posted=" + posted + ", permission=" + permission + "]";
	}
	

	
	
	

}
