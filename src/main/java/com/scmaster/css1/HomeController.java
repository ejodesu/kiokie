package com.scmaster.css1;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang.NumberUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scmaster.css1.dao.LocalDAO;
import com.scmaster.css1.vo.Todoufuken;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	LocalDAO ldao;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "prologue";
	}
	
	@RequestMapping(value={"localView","localView/@{comlatlng:.+}"},method=RequestMethod.GET)
	public String homeView(Model model, Optional<String> lname, @PathVariable Optional<String> comlatlng){
		String lname_val = null;
		if(lname.isPresent()){
			lname_val = lname.get();
			logger.info(lname_val);
		}	
		
		if(comlatlng != null && comlatlng.isPresent()){
			String[] latlng = comlatlng.get().split(",");
			
			if(latlng!=null) {
				System.out.println(comlatlng.get());
				System.out.println(latlng[0]);
				System.out.println(latlng[1]);
				if(latlng.length ==2 && latlng[0].matches("\\d*\\.?\\d*") && latlng[1].matches("\\d*\\.?\\d*")){
					model.addAttribute("lat",latlng[0]);
					model.addAttribute("lng",latlng[1]);
				} else if(latlng.length ==3 && latlng[0].matches("\\d*\\.?\\d*") && latlng[1].matches("\\d*\\.?\\d*") && latlng[2].matches("\\d+z")){
					model.addAttribute("lat",latlng[0]);
					model.addAttribute("lng",latlng[1]);
					model.addAttribute("zoom",latlng[2].substring(0,Math.max(latlng[2].length()-1,0)));
				}
			}
		}
		
		if(lname_val==null){
			return "local";			
		}else{
			Todoufuken local = ldao.searchLocalByName(lname_val);
			logger.info(local.getLname());
			model.addAttribute("searchloc", local);
			return "local";
		}
	}
	
	@RequestMapping(value="totalView", method=RequestMethod.GET)
	public String totalView(){
		return "main";
	}

	@RequestMapping(value="prologue", method=RequestMethod.GET)
	public String prologue(){
		return "prologue";
	}
	
}
