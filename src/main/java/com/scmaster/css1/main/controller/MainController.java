package com.scmaster.css1.main.controller;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scmaster.css1.dao.TweetDAO;
import com.scmaster.css1.vo.Livefeed;
import com.scmaster.css1.vo.Tweet;


@Controller
@RequestMapping(value="kioki")
public class MainController {

	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
		/*@RequestMapping(value="homeView",method=RequestMethod.GET)
		public String homeView(){
			return "home";
		}
		
		@RequestMapping(value="totalView", method=RequestMethod.GET)
		public String totalView(){
			return "main";
		}*/
	
	
	@Autowired
	TweetDAO tdao;
	
	
	/*Twitter 불러오기*/
	@RequestMapping(value="getTwitter")
	public @ResponseBody ArrayList<Tweet> tweetList(){
		ArrayList<Tweet> tweet = null;
		logger.info("트위터 불러오기 시작");
		tweet = tdao.selectTwitter();
		System.out.println(tweet);
		
		logger.info("트위터 불러오기 종료");
		return tweet;
	} 

	
	
	
}
