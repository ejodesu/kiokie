package com.scmaster.css1.main.controller;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scmaster.css1.dao.LocalDAO;

@RequestMapping(value="news")
@Controller
public class NewsController {
	
	@Autowired
	LocalDAO ldao;
	private static final Logger logger = LoggerFactory.getLogger(NewsController.class);
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public @ResponseBody ArrayList<HashMap<String,Object>> getNewsList(String title){
		
		logger.info("뉴스 읽어오기 시작");
	
		ArrayList<HashMap<String,Object>> newslist = new ArrayList<HashMap<String,Object>>();
		
		newslist = ldao.readNews(title);
		
		
		logger.info("뉴스 읽어오기 종료");	
		
		return newslist;
	}		
	@RequestMapping(value="view",method=RequestMethod.GET)
	public @ResponseBody HashMap<String,Object> getNewsArticle(){
		
		return null;
	}
}
