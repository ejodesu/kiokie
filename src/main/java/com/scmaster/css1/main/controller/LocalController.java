package com.scmaster.css1.main.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import javax.servlet.http.HttpServlet;

import net.sf.json.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scmaster.css1.dao.LocalDAO;
import com.scmaster.css1.vo.POI;
import com.scmaster.css1.vo.Todoufuken;
import com.scmaster.css1.vo.Weather;

@Controller
public class LocalController {
	//로컬 맵에 관련된 컨트롤러는 여기로 옵시다!
	
	@Autowired
	LocalDAO ldao;
	
	private static final Logger logger = LoggerFactory.getLogger(LocalController.class);
	
	/*장소 검색 유무여부*/
	@ResponseBody
	@RequestMapping(value="localCheck", method=RequestMethod.GET)
	public String listKenAc(String lname){
		String result = "true";
		Todoufuken local = ldao.searchLocalByName(lname);
		if(local == null){
			result ="false";
		}
		return result;
	}

	/*점 단위 POI 검색*/
	@ResponseBody
	@RequestMapping(value="localView/searchPoint", method=RequestMethod.GET)
	public ArrayList<POI> searchPoint(double lat, double lng, int type,
			Optional<Integer> distance, Optional<Integer> shelterType){
		logger.info("포인트 검색 시작");
		HashMap<String, Object> loc = new HashMap<String, Object>();
		ArrayList<POI> hospitalList = new ArrayList<POI>();
	     loc.put("lat", lat);
		 loc.put("lng", lng);
		 loc.put("type", type);
		 if(shelterType.isPresent()){
			 loc.put("sheltertype", shelterType.get());
		 }
		 if(distance.isPresent()){
			 loc.put("distance", distance.get());
		 }
		hospitalList = ldao.searchPOIByPoint(loc);
		logger.info("포인트 검색 종료");
		return hospitalList;
	}
	
	/*범위 단위 POI 검색*/
	@ResponseBody
	@RequestMapping(value="localView/searchRect", method=RequestMethod.GET)
	public ArrayList<POI> searchRect(double lat, double lng, int type, 
			double southlat, double northlat, double westlng, double eastlng, Optional<Integer> sheltertype){
		logger.info("범위 검색 시작");
		HashMap<String, Object> loc = new HashMap<String, Object>();
		ArrayList<POI> hospitalList = new ArrayList<POI>();
		loc.put("lat", lat);
		 loc.put("lng", lng);
		 loc.put("type", type);
		 if(sheltertype.isPresent()){
			 loc.put("sheltertype", sheltertype.get());
		 }
		loc.put("southlat", southlat);
		loc.put("northlat", northlat);
		loc.put("westlng", westlng);
		loc.put("eastlng", eastlng);
		
		hospitalList = ldao.searchPOIByRect(loc);
		logger.info("범위 검색 종료");
		return hospitalList;
	}	
	
	/*날씨 검색*/
	@ResponseBody
	@RequestMapping(value="localView/searchWeather", method=RequestMethod.GET)
	public ArrayList<Weather> searchWeather(){
		logger.info("날씨 정보 검색 시작 : Controller");

		ArrayList<Weather> weatherList = new ArrayList<Weather>();
		/*Dao 작업부분*/	
		weatherList = ldao.searchWeather();
		System.out.println(weatherList);
			
		logger.info("날씨 정보 검색 완료 : Controller to Local View");
		return weatherList;
	}
	
	/*알림 검색*/
	@ResponseBody
	@RequestMapping(value="localView/selectAlert", method=RequestMethod.GET)
	public ArrayList<HashMap<String,Object>> selectAlert(){
		logger.info("알림 검색 시작 : Controller");

		ArrayList<HashMap<String,Object>> result = new ArrayList<>();
		/*Dao 작업부분*/	
		result = ldao.selectAlert();
		System.out.println(result);
			
		logger.info("알림 검색 완료 : Controller to Local View");
		return result;
	}
	
	@RequestMapping(value="weatherscore",method=RequestMethod.GET)
	public @ResponseBody String getWeatherScore(String title){		
		logger.info("점수 읽어오기 시작");
		String result = null;		
		result = ldao.selectWeatherScore(title);		
		logger.info("점수 읽어오기 종료");		
		return result;
	}	
}
