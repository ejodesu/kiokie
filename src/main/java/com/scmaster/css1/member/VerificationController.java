package com.scmaster.css1.member;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scmaster.css1.common.TempKey;

@Controller
@RequestMapping(value="")
public class VerificationController {
	
	@Autowired
	private JavaMailSender mailSenderObj;
	
	@RequestMapping(value="send", method=RequestMethod.GET)
	public String sendMail(String toUser){
		System.out.println("send");
		
		TempKey tk = new TempKey();
		String generated = tk.getKey(7, false); //DB에 저장하고 저장 시간과 해당 유저 이름을 기록하여 유효 시간 내에 입력했는지 판별하여 가입을 승인시킨다. 
		
		if(toUser.isEmpty()){
			System.out.println("올바르지 않은 수신자.");
			return "redirect:/";
		}
		
		mailSenderObj.send(new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				try {
				MimeMessageHelper mimeMsgHelperObj = new MimeMessageHelper(mimeMessage, false, "UTF-8");
				mimeMsgHelperObj.setTo(toUser);
				mimeMsgHelperObj.setFrom(new InternetAddress("ejodesu@gamil.com", "KIOKI Safety Hub"));				
				mimeMsgHelperObj.setText(String.format("<h1>환영합니다.</h1><p>회원 가입을 축하합니다. 본인이 맞으신 경우에만 아래 링크를 누르거나, 인증 번호를 가입 페이지에 적어서 가입을 완료하세요.</p><h2>%s</h2><a href=\"%s\" target=\"_blank\">지금 바로 인증</a>", generated, generated), true);
				mimeMsgHelperObj.setSubject("회원 가입 인증 부탁드립니다.");
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		System.out.println("\nMessage Send Successfully.... Hurrey!\n");
		
		return "redirect:/";
	}
}
