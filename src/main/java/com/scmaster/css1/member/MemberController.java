package com.scmaster.css1.member;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.GsonFactoryBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scmaster.css1.dao.LocalDAO;
import com.scmaster.css1.dao.MemberDAO;
import com.scmaster.css1.vo.Member;
import com.scmaster.css1.vo.Todoufuken;



@RequestMapping(value="member")
@Controller
public class MemberController {
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);

	@Autowired
	MemberDAO dao;
	
	@Autowired
	LocalDAO ldao;
	
	/*회원가입 화면 이동*/	
	@RequestMapping(value="joinForm")
	public String joinForm(){
		logger.info("회원 가입 화면 이동 시작");		
				
		logger.info("회원 가입 화면 이동 종료");
		
		return "member/joinForm";
	}
	
	
	/*회원가입*/
	@RequestMapping(value="join", method=RequestMethod.POST)
	public String Join(Member member){
		logger.info("회원가입 시작");
		System.out.println(member);
		dao.insertMember(member);
		logger.info("회원가입 종료");
		return "member/loginForm";
	}
	
	/*로그인 화면 이동*/
	@RequestMapping(value="login", method=RequestMethod.GET)
	public String loginForm(){
		logger.info("로그인폼이동");
	return "member/loginForm";
	}
	
	
	/*아이디 중복확인*/	
	@RequestMapping(value = "checkId", method = { RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody String idCheck(String username){
		
		Member member = dao.searchMember(username);		
		
		return member==null?"ok:"+username:"exist:"+username;
    }
	
	@ResponseBody
	@RequestMapping(value="listken", method=RequestMethod.GET)
	public ArrayList<HashMap<String, Object>> listKen(Model model){
		ArrayList<HashMap<String, Object>> kenlist = null;
		kenlist =dao.listTodohuken();
		//lno and lname 만 가져와서 보냄		
		System.out.println(kenlist);		
		
		return kenlist;
	}
	
	/*자동완성용 도도부현리스트*/
	@ResponseBody
	@RequestMapping(value="listkenAc", method=RequestMethod.GET)
	public ArrayList<HashMap<String,Object>> listKenAc(Model model){
		ArrayList<HashMap<String,Object>> kenlist = null;
		kenlist =dao.listken();
		//lno and lname 만 가져와서 보냄		
		System.out.println(kenlist);			
		return kenlist;
	}
	
	@ResponseBody
	@RequestMapping(value="listshiku", method=RequestMethod.GET)
	public ArrayList<HashMap<String, Object>> listShiku(int value){
		ArrayList<HashMap<String, Object>> shikulist = null;
		
		shikulist = dao.listShiku(value);
		if(shikulist != null){
			HashMap<String,Object> noSelect = new HashMap<>();
			noSelect.put("name", "(無し)");
			noSelect.put("value", 0);
			noSelect.put("selected", true);
			shikulist.add(0,noSelect);
		}
		System.out.println(shikulist);
		return shikulist;
	}

	
	/*로그인 하기*/ 
	@ResponseBody
	@RequestMapping(value="login", method=RequestMethod.POST)
	public String login(@RequestBody Member memberEx,HttpSession session){
		logger.info("로그인 시작");
		logger.info(memberEx.getUsername());
		
		String result = "true";
		Member member = dao.selectMember(memberEx);
		if(member!=null){
			logger.info(member.toString());
		} else {
			logger.info("no member with pass: "+memberEx.getUsername());
		}
		 
		if (member != null){
			logger.info("로그인유저정보 불러오기 시작");
			System.out.println(member.getLno());
			Todoufuken local = ldao.seacrhLocal(member.getLno());
			System.out.println(local.getLno());
			session.setAttribute("loginlocal",local);
			session.setAttribute("loginLat", member.getLatitude());
			session.setAttribute("loginLng", member.getLongitude());
			session.setAttribute("loginUser", member.getUsername());
			session.setAttribute("loginNum", member.getUsernum());
			logger.info("로그인 성공 종료");
			return result;
		}
		else{
			logger.info("로그인 실패 종료");
			result = "false";
			return result;
		}
	}
	
	//로그아웃
		@RequestMapping(value="logout",method=RequestMethod.GET)
		public String logout(HttpSession session){
			logger.info("로그아웃 시작");
			session.removeAttribute("loginUser");
			session.removeAttribute("loginLat");
			session.removeAttribute("loginLng");
			session.removeAttribute("loginlocal");
			session.removeAttribute("loginNum");
			logger.info("로그아웃 종료");
			return "redirect:/";
		}
		
	//회원정보변경 폼 이동
		@RequestMapping(value="editUserForm", method=RequestMethod.GET)
		public String EditUserForm(String username, Model model){
			logger.info("회원정보폼 이동 시작");
			//회원정보 갖고와서 회원 셀렉 하나 가져와서 회원정보변경 view에 뿌리기
			Member member = dao.searchMember(username);
			System.out.println(member.getEmail());
			model.addAttribute("loginMember", member);
			logger.info("회원정보폼 이동 종료");
			return "member/editUserForm";
		}
		
		//gohome		
		@RequestMapping(value="gohome", method=RequestMethod.GET)
		public String goHome() {
			
			return "redirect:/";
		}
		

	
	//회원정보수정
		@RequestMapping(value="editMember", method=RequestMethod.POST)
		public String editUser(Member member){
			logger.info("회원정보수정 시작");
			dao.editMember(member);
			logger.info("회원정보수정 종료");
			return "redirect:logout";
		}
	

}
