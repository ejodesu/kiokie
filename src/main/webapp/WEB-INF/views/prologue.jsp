<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/semantic/dist/semantic.min.css">
	  <script
	   	src="${pageContext.request.contextPath}/resources/jquery/jquery-3.3.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/semantic/dist/semantic.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/semantic-ui-search/search.min.js"></script>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/semantic-ui-search/search.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/w3css/w3.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/w3css/w3-theme-blue-grey.css">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="https://fonts.googleapis.com/css?family=Julius+Sans+One|Nanum+Gothic&amp;subset=korean" rel="stylesheet">
	<script src="${pageContext.request.contextPath}/resources/jquery/jquery.cycle2.min.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
	html,body,h1,h2,h3,h4,h5,a{
		font-family: 'Nanum Gothic', sans-serif;
		font-family: 'Julius Sans One', sans-serif;}
	.w3-jumbo{
		color: #ff5722;
	}
	.green-text{
		color: #8c9900;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#mainBtn').on('click', mainForm);
		$('#searchBtn').on('click', localForm);
		$.ajax({
			url :'member/listkenAc',
			type: 'GET',
			dataType : 'JSON',				
			success : function(data){
				console.log(data);
				/* var availableTags = data; */
				$('.ui.search').search({
					source: data,
		            description : 'description',
		            searchFields: ['title','description','custom'],
		            onSelect: function(result, response) {
		            	localForm(result);
		            }
					});
			},
			error : function(){
				alert('error');
			}		
		});
	});
	
	function mainForm(){
		location.href = "${pageContext.request.contextPath}/totalView";
	}
	
	function localForm(keyword){
		if(typeof keyword == "undefined"){
			var searchloc = $('#tags').val();
		}else{
			var searchloc = keyword['title'];
		}
		
		/* 공란 */
		if(searchloc.length ==0){
			alert("공란입니다");
			return;
		}
		//alert(searchloc);
		
		/* 있는 장소인지 검색 */
		$.ajax({
			url: "localCheck",
			type: "get",
			data: {
				lname : searchloc
			},
			dataType : "text",
			success: function(result){
				if(result=="true"){
					location.href = "${pageContext.request.contextPath}/localView?lname="+searchloc;
				}else{
					alert("Not found location");
				}
			},
			error: function(e){
				alert("Fail");
				console.log(e)
			}
		})
		
	}
</script>
  <body>
  
  <!-- Sidebar with image -->
	<nav class="w3-sidebar w3-hide-medium w3-hide-small" style="width:50%;" >
	  <!-- The Grid -->
	  <div class="cycle-slideshow" style="max-width:1800px; height: 100%">
		  <img src="${pageContext.request.contextPath}/resources/img/prologue1.jpg" style="width:100%; height: 100%;">
		  <img src="${pageContext.request.contextPath}/resources/img/prologue4.jpg" style="width:100%; height: 100%;">
		  <img src="${pageContext.request.contextPath}/resources/img/prologue2.jpg" style="width:100%; height: 100%;">
		  <img src="${pageContext.request.contextPath}/resources/img/prologue3.jpg" style="width:100%; height: 100%;">
		  <img src="${pageContext.request.contextPath}/resources/img/prologue5.jpg" style="width:100%; height: 100%;">
	  </div>
	</nav>
  <!-- fluid category w3-padding-large w3-margin-top -->
  <!-- Page Content -->
	<div class="w3-main w3-padding-large" style="margin-left:50%;" >
		<!-- Header -->
		  <header class="w3-container w3-center w3-animate-top" style="padding:128px 16px" id="home">
		    <h1 class="w3-jumbo "><b>KIOKI</b></h1>
		    <p class="green-text">KIOKI Local Safety Hub</p>
		    <!-- search -->
		    <div class="ui search" style="margin-bottom: 10px;">
			  <div class="ui icon input">
			    <input class="prompt" type="text" id="tags" placeholder="Search local">
			  </div>
			  <div class="results"></div>
			</div>
		    <!-- without search -->
		    <div class="positive ui animated button" id="mainBtn" tabindex="0" style="width:200px;">
			  <div class="visible content" >メイン</div>
			  <div class="hidden content">
			    <i class="right arrow icon"></i>
			  </div>
			</div>
		  </header>
	
	  <!-- End Grid -->
	      
  <!-- End Page Container -->
	</div>
  </body>
  </html>
