<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<title>KIOKI Safety Hub</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/semantic/dist/semantic.min.css">
<script src="${pageContext.request.contextPath}/resources/jquery/jquery-3.3.1.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/semantic/dist/semantic.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/semantic-ui-search/search.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/kioki.js?v=2"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/semantic/Semantic-UI-Alert.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/semantic-ui-search/search.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/w3css/w3.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/w3css/w3-theme-blue-grey.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/kiokicss/kioki.css?v=1">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Julius+Sans+One|Nanum+Gothic&amp;subset=korean" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/semantic/Semantic-UI-Alert.css">
<script type="text/javascript">
	$(function(){
		$('#logoutBtn').on('click', logoutForm);
		$('#logout').on('click', logout);
	});
		
	function logout(){
		location.href = "${pageContext.request.contextPath}/member/logout";
	}
	
	function logoutForm(){
		$('#logoutDiv')
		  .modal({
			blurring: true
			})
		  .modal('show')
		;
	}
</script>
 	
<body class="w3-theme-l5">

<!-- Navbar -->
<div class="w3-top">
 <div class="w3-bar w3-theme-d2 w3-left-align w3-large" >
  <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
  <a href="${pageContext.request.contextPath}/totalView" class="w3-bar-item w3-button w3-padding-large w3-theme-d4 w3-hover-white"><i class="home icon"></i>KIOKI</a>
  <a href="${pageContext.request.contextPath}/localView" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="Account Settings"><i class="map marker alternate icon"></i></a>
  <a href="${pageContext.request.contextPath}/livefeed/livefeedForm" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="livefeed"><i class="newspaper icon"></i></a>
  
  <c:if test="${sessionScope.loginUser != null}">
  	<a id="logoutBtn" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="Logout">
		<i class="power off icon"></i>
	</a>
  	<a href="${pageContext.request.contextPath}/member/editUserForm?username=${sessionScope.loginUser}" id="EditUserForm" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="Edit">
	  	<i class="edit outline icon"></i>
	</a>
  </c:if>
  <c:choose>
  	<c:when test="${sessionScope.loginUser == null }">
  		<a href="${pageContext.request.contextPath}/member/login" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="User">	
  			<i class="user circle icon"></i> LOGIN
  		</a>
  	</c:when>
  	<c:otherwise>
  		 <a href="#" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="User">
  		 	<i class="user circle icon"></i>${sessionScope.loginUser }
  		 </a>
  	</c:otherwise>
  </c:choose>
 </div>
</div>

	<!-- logout -->
	<div id="logoutDiv"class="ui basic tiny modal">
	  <div class="ui icon header">
	    <i class="power off icon"></i>
	    Logout
	  </div>
	  <div class="content">
	    <p>honttoni demasuka</p>
	  </div>
	  <div class="actions">
	    <div class="ui red basic cancel inverted button">
	      <i class="remove icon"></i>
	      No
	    </div>
	    <div id="logout" class="ui green ok inverted button">
	      <i class="checkmark icon"></i>
	      Logout
	    </div>
	  </div>
	</div>

<!-- Page Container -->
<div class="w3-container w3-content kiokie-wrapper">  