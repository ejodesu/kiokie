<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="../inc/header.jsp"%>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="${pageContext.request.contextPath}/resources/jquery/jquery.ui.widget.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="${pageContext.request.contextPath}/resources/jquery/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="${pageContext.request.contextPath}/resources/jquery/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="${pageContext.request.contextPath}/resources/jquery/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="${pageContext.request.contextPath}/resources/jquery/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="${pageContext.request.contextPath}/resources/jquery/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="${pageContext.request.contextPath}/resources/jquery/jquery.fileupload-image.js"></script>
<script type="text/javascript">
function isVideo(name){
	return name.endsWith(".mp4") || name.endsWith(".avi");
}

$(function () {
	
	
	
	//업로드란
	$('#guide1').on('click',guide1);
	$('#guide2').on('click',guide2);
	$('#upProgress').hide();
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
        	$('#upProgress').hide();
            $.each(data.result.files, function (index, file) {
            	$('#output').append('<'+(isVideo(file.url) ? 'video muted autoplay' : 'img')+' src=${pageContext.request.contextPath}/livefeed/files/'+file.url+' width=200px height=200px style=cursor:pointer; onclick="'+(isVideo(file.url) ? 'this.paused?this.play():this.pause();' : 'window.open(\'${pageContext.request.contextPath}/livefeed/files/'+file.url+'\')')+'">'+(isVideo(file.url) ? '</video>' : ''));
            	$('<br/>').appendTo('#output');
                $('<p/>').text(file.name).appendTo($('#output'));
                var beforeUrl = $('#fileurl').val();
                $('#fileurl').val(!beforeUrl ? file.url : beforeUrl+';'+file.url);
                
            });
        },
        progressall: function (e, data) {
        	$('#upProgress').show();
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#upProgress').progress({
            		percent: progress
        	});
        },
        disableImageResize: false,
        imageMaxHeight: 400,
        imageCrop: false //Force cropped images
    }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');
    $('#lbFile').click(function(){
        $('#fileupload').click();
        return false;
    });
    getAlert("<c:url value='/' />");
});

	function clearWriteform(){
		$('#livefeedcontent').val('');
		$('#local').dropdown('restore defaults')
		$('#localinner').dropdown('restore defaults')
		$('#fileurl').val('');
		$('#output').html('');
	}

	
	//guide1
	function guide1(){
		$('#guideDiv')
		.modal('setting', 'transition', 'horizontal flip')
        .modal('show')
	}
	
	//guide1
	function guide2(){
		$('#guide2Div')
		.modal('setting', 'transition', 'horizontal flip')
        .modal('show')
	}
	
	function readlivefeed(lno) {

		$.ajax({
			url: 'getlivefeed',
			type: 'GET',
			data: {
				lno: lno
				},
			contentType: 'application/JSON; charset=utf-8',
			success: function (data) {

				var livefeedlist = data;

				var livefeed = '';

				$.each(livefeedlist, function (key, value) {
					/*no라이브피드번호 usernum유저아이디 lno 로컬 cno 도시 content 라이브피드내용
					posted 글 쓴 내용 attachtype 첨부파일 양식 attach 첨부파일*/

					livefeed += '<div class="ui raised segment feed">';
					livefeed += '<div class="ui ribbon label" style="background-color:#';
					livefeed += value.color
					livefeed += '; color: white; font-size: 15px;">'+ value.lname + ' ' + value.clname+ '</div>';
					livefeed += '<div class="content card" style="width:400xp; margin-top: 10px;">';
					livefeed += '<div class="summary">';
					livefeed += '<div class="date" style="font-size:10pt">';					
					livefeed += ' '+ value.posted + ''; //시간
					livefeed += '</div>';
					livefeed += '<a class="user" style="font-size:15pt; font-weight: bold;">';
					livefeed += '' + value.username + ''; /*username*/
					livefeed += '</a><br>';
					livefeed += '<a class="user" style="font-size:15pt; font-weight: bold;">';
					livefeed += '</a>' + value.content + ''; /*내용*/
					livefeed += '<div class="ui right floated labeled button" tabindex="0"">';
					livefeed +=	'<div class="ui negative button" onclick="report('+value.usernum+')">';
					livefeed += '<i class="bullhorn icon"></i>'; 
					livefeed += 'Dislike';
					livefeed += '</div>';
					livefeed += '<a class="ui basic red left pointing label" style="z-index:0">';
					livefeed +=  value.trycount;
					livefeed += '</a>';
					livefeed += '</div>';
					livefeed += '<div style="margin-bottom: 10px;">'
					/*업로드된 이미지 표시*/
					var img = value.attach;
					if(img !=null){
					var imgsplit = img.split(';');			
					  	for ( var i in  imgsplit ) {
							livefeed += '<'+(isVideo(imgsplit[i]) ? 'video controls muted autoplay' : 'img')+' src="${pageContext.request.contextPath}/livefeed/files/'+imgsplit[i]+'" style="display:inline;clear:none;max-height:120px;" onclick='+(isVideo(imgsplit[i]) ? 'this.paused?this.play():this.pause();' : 'window.open(\'${pageContext.request.contextPath}/livefeed/files/'+imgsplit[i]+'\')')+' />';   
						}
					}
					livefeed += '</div>';
					livefeed += '</div>';
					livefeed += '</div>';
					livefeed += '</div>'; //지역
					livefeed += '<div style="height:35px">';
					livefeed += '</div>';
										
					$('#livefeedlist').html(livefeed);

				});
				//innerHTML
				
				/*하루가 지난 뉴스피드 삭제*/
				/* 	var now = new Date();
					var oneDay = 60 * 60 * 24 * 1000;				
					
					
					if(value.posted >= now.getTime() + oneDay){
					 	continue;			
					} */
		
			   
				
			} //success		

		}); //ajax

	}


	/*신고하기 기능*/
	function report(usernum){
	 	var loginId = "<%=session.getAttribute("loginNum")%>";	
	 	if(loginId =="null"){			
			alert("로그인해주세요");
			return;
		}	 	
		confirm("정말로 신고하시겠습니까? 허위신고인 경우 불이익을 받을 수 있습니다");
		$.ajax({
			url : 'report',
			type : 'GET',
			dataType :'JSON',
			data:{
				'usernum' : usernum			
			},
			success: function(){
				alert('신고되었습니다');
				readlivefeed();				
			},error : function() {
                alert("이미 신고한 유저입니다");
                readlivefeed();
           }			 
		 });		
	}	

	$(document).ready(function () {

		readlivefeed();		

		/*livefeed 쓰기*/
		$('#writelivefeed').click(function () {
			var livefeedcontent = $('#livefeedcontent').val();
			var posted = new Date();
			var lno = $('#local').dropdown('get value');
			var cno = $('#localinner').dropdown('get value');
			var fileurl = $('#fileurl').val();

			$.ajax({
				url: 'insertlivefeed',
				type: 'post',
				dataType: 'json',
				data: {
					'usernum': 0,
					/*usernum '0' will be set in Livefeedcontroller*/
					'lno': lno,
					'cno': cno,
					'content': livefeedcontent,
					'posted': posted,
					'attachtype': 0,
					/*attachtype is not used*/
					'attach': fileurl

				},
				success: function () {
					readlivefeed();
					clearWriteform();
				},
				failed: function() {
					console.log('write failed');
				}
			});

		});
		
		/*현재위치 */

		$('#btnPos')
			.click(
				function () {
					if ("geolocation" in navigator) {
						/* geolocation is available */
						$('#ploc').html(
							"Checking location info...");
						navigator.geolocation
							.getCurrentPosition(function (
								position) {
								/*($('#ploc')
									.html(
										"당신은 위도 " +
										position.coords.latitude +
										", 경도 " +
										position.coords.longitude +
										"에 있습니다.");*/
								codeGeo(position.coords.latitude,position.coords.longitude,this,function(data){
									if(!"status" in data || !data["status"] || data["status"]!="OK"){
										$('#ploc').html("No location data.");
										console.log(data);
										return;
									}
									data["result"][0]["address_components"].forEach(function(e){
										//locality
										//administrative_area_level_1
									
										e.type
										//여기서 더 이어가야 합니다.
									});									
									
									$('#ploc').html("You are at " +
										data);
								});
							});
					} else {
						/* geolocation IS NOT available */
						$('#ploc')
							.html(
								"Your browser is too old to use location service.");
					}
				});

		//dropbox >list ken shiku	
		$.ajax({
			url: 'listken',
			type: 'GET',
			dataType: 'JSON',
			success: function (data) {
				$('#local').dropdown({
					onChange: function (val, name) {
						$.ajax({
							url: 'listshiku',
							type: 'GET',
							dataType: 'JSON',
							data: {
								value: val
							},
							success: function (shiku_data) {
								$('#localinner').dropdown({
									values: shiku_data,
									placeholder: '市区を選んでください'
								});
								//$('#localinner').dropdown('set text','(無し)');
							},
							error: function () {
								//alert('시/구 목록을 가져오지 못함');
								console.log('시/구 목록을 가져오지 못함');
							}
						});
					},
					placeholder: '都道府県',
					values: data
				});
			},
			error: function () {
				console.log('no ken');
			}

		});

	//drop box location filter
	$.ajax({
			url: 'listken',
			type: 'GET',
			dataType: 'JSON',
			success: function (data) {
				data.splice(0,0, {name:'全国', value:0});
				
				$('#localfilter').dropdown({
					onChange : function(value){
						readlivefeed(value);
					}, 
					placeholder: '<i class="filter icon"></i> 都道府県 Filter',
					values: data
				});
				$('#localfilter').dropdown('set selected', 0);
			},
			error: function () {
				alert('error');
			}
		});

	}); //ready function

	// Accordion >>>right column
	function myFunction(id) {
		var x = document.getElementById(id);
		if (x.className.indexOf("w3-show") == -1) {
			x.className += " w3-show";
			x.previousElementSibling.className += " w3-theme-d1";
		} else {
			x.className = x.className.replace("w3-show", "");
			x.previousElementSibling.className =
				x.previousElementSibling.className.replace(" w3-theme-d1", "");
		}
	}
</script>

<!-- The Grid -->
<div class="w3-row">

	<!-- Left Column -->
	<div class="w3-col m3">
		<br>
		
		<!-- 위치 필터 -->
		<div class="ui fluid selection dropdown" id="localfilter" style="background-color: #ff5722;">
			<input type="hidden">
			<i class="dropdown icon" style="color: white;"></i>
			<div class="default text" style="color: white;"><i class="filter icon"></i>都道府県 Filter</div>
			<div class="menu">
				<div class="item" data-value="1">Test</div>
				<div class="item" data-value="0">Test</div>
			</div>
		</div>
		
		<!-- Alert Box -->
		<div class="ui red segment w3-animate-left" style="border-color: #d32f2f;" id="alertbox" style="display:none;">
			<div class="ui red ribbon label" style="font-size:15px;">
        		<i class="exclamation circle icon"></i> お知らせ
      		</div>
			<p>TEXT</p>
		</div>

		<!-- End Left Column -->
	</div>

	<!-- Right Column -->
	<div class="w3-col m2" style="float:right">
		<br>
		<!-- menu -->
		<div class="w3-card w3-round">
			<div class="w3-white">
				<button id="guide1" class="w3-button w3-block w3-theme-l1-navy w3-left-align w3-hover-blue-grey">
					<i class="question circle icon"></i> フィード 作成</button>
				<button id="guide2" class="w3-button w3-block w3-theme-l1-navy w3-left-align w3-hover-blue-grey">
					<i class="question circle icon"></i> フィードリスト</button>
			</div>
		</div>
		<br>
		<!-- End Right Column -->
	</div>

	<div class="w3-col m6" style="margin:30px;" style="float:center">

		<!--  livefeed -->
		<div class="ui feed " id="livefeed">
		
			<c:if test="${sessionScope.loginUser!=null}">
				<div class="comment">
					<div class="ui reply olive segment form" style="margin-bottom:30px;">
						<!-- 라이브피드 입력란 -->
						<div class="field">
							<textarea id="livefeedcontent" style="height: 100px"></textarea>							
						</div>
						<!-- 이미지 파일 업로드 -->
							<div id="drop-area">
						  <!--<label for="myFile">파일선택 : </label>
						  <input type="file" accept="image/*" id="myFile" onchange="loadFile(event)" multiple><br/>-->
						  <div>

						  <input id="fileupload" type="file" accept="image/*, video/mp4" name="files[]" data-url="uploadProcess" multiple style="display:none;">
						  </div>
						  <input type="hidden" id="fileurl"/>
						  <div class="ui indicating progress" id="upProgress">
							  <div class="bar"></div>
							  <div class="label">Uploading</div>
						  </div>
						  
						<!-- 업로드 파일 미리 보기 위치 -->	  
							  <div id="output">
    					<!-- 	  <input type="submit" value="확인"> -->
							</div>
						
						<p id="ploc" style="margin-bottom: 0px; font-size: small;">Please select your location.</p>
						<!-- 위치 선택 input -->
						<div class="ui selection dropdown" id="local">
							<input type="hidden">
							<i class="dropdown icon"></i>
							<div class="default text">Todofuken</div>
							<div class="menu">
								<div class="item" data-value="1">Test</div>
								<div class="item" data-value="0">Test</div>
							</div>
						</div>
						<div class="ui selection dropdown" id="localinner" style="margin-right: 30px;">
							<input type="hidden">
							<i class="dropdown icon"></i>
							<div class="default text">Shiku</div>
							<div class="menu">
								<div class="item" data-value="1">Test</div>
								<div class="item" data-value="0">Test</div>
							</div>
						</div>
						<!-- 현재위치 검색 -->
						<button class="ui circular icon red button" id="btnPos" >
							<i class="map pin icon"></i>
						</button>
						<!-- 라이브 피드 전송 -->
						<label for="file" class="ui circular yellow icon button" id="lbFile" >
						    <i class="file image icon"></i>
						</label>
						<div class="ui primary submit circular icon button" id="writelivefeed" >
							<i class="icon edit"></i> write
						</div>
				</div>
			</c:if>
		</div>
		
		<!-- list -->
		<div class="ui olive segment" id="livefeedlist" >

		</div>
		<br>
		<!-- livefeed end -->

	</div>
	
	<!-- guide1 modal -->
    <div id="guideDiv" class="ui longer modal">
	  <i class="close icon"></i>
	  <div class="header">
	    <i class="question circle icon"></i> フィード 作成
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide13.PNG"><br>

	      	      
	    </div>
	    <div class="description">
	      <div class="ui header">
	      	<p>작성 가이드</p>
	      	<p>로그인 상태에서만 글 작성이 가능합니다</p>
	      	<p>1. 사용중인 브라우저가 현재 위치 기능이 있는지 확인합니다</p>
	      	<p>2. 파일과 동영상 첨부가 가능합니다</p>
	      	<p>3. 현재 주변의 상황을 알려주세요(사진, 동영상첨부 가능)</p>
	    
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- guide2 modal -->
    <div id="guide2Div" class="ui longer modal">
	  <i class="close icon"></i>
	  <div class="header">
	    <i class="question circle icon"></i> フィードリスト
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide14.PNG">
	    </div>
	    <div class="description">
	      <div class="ui header">
	      	작성된 피드의 위치를 표시합니다
	      </div>
	    </div>
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide15.PNG">
	    </div>
	    <div class="description">
	    	<div class="ui large image">
	      		<div class="ui header">
	      			DISLIKE를 통해 부정확하거나 불건전한 피드를 게시한 사용자의 이용을 제한할 수 있습니다
	      		</div>
	      	</div>
	    </div>
	  </div>
	</div>
	
	
	<!-- End Grid -->
</div>

<%@ include file="../inc/footer.jsp"%>