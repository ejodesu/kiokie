<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="../inc/header.jsp"%>
<script>
	var validId = false, validPw = false, validEmail = false;

	/*아이디 체크하여 가입버튼 비활성화, 중복확인/사용가능 메세지 비동기적 출력*/
	$(document).ready(function() {
				var inputed = $('#username').val();
				if (inputed.length == 0) {
					$('#idconfirm').css('color', '#FF0000');
					$('#idconfirm').text('필수입력사항입니다');
				}

				//날짜 필드 준비
				$('#birth-visible')
						.calendar({
									monthFirst : true,
									type : 'date',
									maxDate : new Date(),
									text : {
										days : [ '일', '월', '화', '수', '목', '금',
												'토' ],
										months : [ '1월', '2월', '3월', '4월',
												'5월', '6월', '7월', '8월', '9월',
												'10월', '11월', '12월' ],
										monthsShort : [ '1', '2', '3', '4',
												'5', '6', '7', '8', '9', '10',
												'11', '12' ],
										today : '오늘',
										now : '지금',
										am : '오전',
										pm : '오후'
									},
									onChange : function(date, text, mode) {
										changeSubmit(true);
									}
								});
				
				$.ajax({
					url :'listken',
					type: 'GET',
					dataType : 'JSON',				
					success : function(data){
						$('#local').dropdown({
							onChange: function (val,name){
								$.ajax({
									url :'listshiku',
									type: 'GET',
									dateType :'JSON',
									data: {
										value: val
									},
									success : function(shiku_data){
										$('#localinner').dropdown({
											values: shiku_data
											,placeholder: '市区を選んでください'
										});
										//$('#localinner').dropdown('set text','(無し)');
									},
									error: function(){
										//alert('시/구 목록을 가져오지 못함');
										console.log('시/구 목록을 가져오지 못함');
									}
								});
							}
							,placeholder: '都道府県'
							,values: data });					
						
					},
					error : function(){
						console.log('no ken');
					}		
					
				});			
					
				
			});

	Date.prototype.yyyymmdd = function() {
		var mm = this.getMonth() + 1; // getMonth()는 0부터 시작
		var dd = this.getDate();

		return [ this.getFullYear(), (mm > 9 ? '' : '0') + mm,
				(dd > 9 ? '' : '0') + dd ].join('');
	};

	function beforeSubmit() { //beforeSubmit() 임에도 submit 자체를 시행한다. return 값은 무관하다.
		var dt = $('#birth-visible').calendar('get date');
		console.log(dt.yyyymmdd());
		document.getElementById('birth').value = dt.yyyymmdd();
		
		if($('input[name="lno"]').val() == ''){
			alert('도도부현 지역을 선택해주세요.');
			return false;
		}
		
		if($('input[name="cno"]').val() == ''){
			$('input[name="cno"]').val(0);
		}
		
		//return true;
		var todofuken = $('#local').dropdown("get text");
		var shiku = $('#localinner').dropdown("get text");
		console.log();
		geoCode(todofuken,shiku,this,function(data){
			if(typeof data != "object"){
				console.log("data is not object");
			}
			console.log(data);
			var loc = data["results"][0]["geometry"]["location"];
			$('#latitude').val(loc["lat"]);
			$('#longitude').val(loc["lng"]);
			
			document.getElementById('joinForm').submit();
		});
	}

	function checkId() {
		validId = false;
		$('#idconfirm').addClass('loading');
		var inputed = $('#username').val();
		$.ajax({
			data : {
				username : inputed
			},
			url : "checkId",
			success : function(data) {
				if(data == null){
					return;
				}
				var d = data.split(':');
				if(d[1]!=$('#username').val()){
					return;
				}
				$('#idconfirm').removeClass('loading');

				if (data == 'exist') {
					$('#idconfirm').css('color', '#ff5722');
				}

				if (d[0]=='exist') {
					$('#idconfirm').css('color', '#FF0000');

					$('#idconfirm').text('이미 존재하는 아이디입니다');
					validId = false;
				} else if (d[0]=='ok') {
					$('#idconfirm').css('color', '#0000FF');
					$('#idconfirm').text('사용가능한 아이디입니다');
					validId = true;
				}
				changeSubmit();
				
			},
			error: function(data) {
				$('#idconfirm').removeClass('loading');
			}
		});
	}

	//재입력 비밀번호 체크하여 가입버튼 비활성화 또는 맞지않음을 알림.
	function pwcheck() {
		validPw = false;
		var inputed = $('#pw1').val();
		var reinputed = $('#pw2').val();

		/*적절한 비밀번호*/
		if (inputed.length > 8 || inputed.lenght < 20) {
			$('#pw1confirm').css('color', "#8c9900");
			$('#pw1').css('background-color', "#4CAF50");
			$('#pw1confirm').text('올바른 비밀번호입니다');

			/*비밀번호 재입력 창 확인*/
			if (inputed == reinputed) {
				$('#pw2').css('background-color', "#B0F6AC");
				$('#pw2confirm').css('color', "#8c9900");
				$('#pw2confirm').text('비밀번호가 동일합니다');
				validPw = true;
			} else if (inputed != reinputed) {
				$('#pw2confirm').css('color', "#ff5722");
				$('#pw2confirm').text('동일한 비밀번호를 입력해 주세요');
			}
		} else if (inputed.length<8||inputed.length>20) {
			$('#pw1confirm').css('color', "#ff5722");
			$('#pw1confirm').text('8자 이상 20자 이하로 입력해야 합니다');
		}
		changeSubmit();
	}

	function checkEmail() {
		validEmail = false;
		//이메일 유효성 체크
		var email = $('#email').val();

		function email_check(email) {
			var regex = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			return (email != '' && email != 'undefined' && regex.test(email));
		}

		if (email == '' || !email_check(email)) {
			$('#emailconfirm').css('color', "#ff5722");
			$('#emailconfirm').text('올바른 이메일 주소를 입력해주세요');
		} else {
			$('#emailconfirm').css('color', "#8c9900");
			$('#emailconfirm').text('올바른 이메일 주소입니다');
			validEmail = true;
		}
		changeSubmit();
	}

	//보내기 버튼의 활성화/비활성화
	function changeSubmit(firedByCalendar) {
		if (validId
				&& validPw
				&& validEmail
				&& ($('#birth-visible').calendar('get date') != null || firedByCalendar)
				&& $('input[name=sex]:checked', '#joinForm').val() != undefined) {
			console.log("유효. " + validId + "," + validPw + "," + validEmail);
			$('#signupbtn').prop('disabled', false);
			$("#signupbtn").css('background-color', "#4CAF50");
		} else {
			console.log("무효. " + validId + "," + validPw + "," + validEmail);
			$('#signupbtn').prop('disabled', true);
			$('#signupbtn').css('background-color', "#aaaaaa");
		}
	}
	
 	function gohome(){
		confirm("메인 페이지로 돌아가겠습니까");
		 document.location.href = "gohome";
		
 	}
	
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/semantic-ui-calendar/dist/calendar.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/semantic-ui-calendar/dist/calendar.min.css" />
<style type="text/css">
	.text-center{
		text-align: center;
		padding-right: 10px;
	}
	.padding-top-bottom{
		padding-top: 5px;
		padding-bottom: 5px;
	}
	.font-small{
		font-size: small;
	}
</style>
<form action="join" method="POST"
	id="joinForm">
	<!-- 회원가입 -->
	<div align="center">
		<table class="ui raised olive segment" style="margin-top:20px; border-radius: 10px;">
			<!-- id >> username -->
			<tr>
				<th class="text-center">Id</th>
				<td class="padding-top-bottom"><div class="ui input">
						<input type="text" placeholder="id" name="username" id="username"
							oninput="checkId()">
					</div>
					<div id="idconfirm" class="font-small">아이디를 입력해 주세요</div></td>

			</tr>

			<!-- 비밀번호 -->
			<tr>
				<th class="text-center">Password</th>

				<td class="padding-top-bottom"><div class="ui input">
						<input type="password" placeholder="비밀번호를 입력해주세요" name="pass"
							id="pw1" oninput="pwcheck()">
					</div>
					<div id="pw1confirm" class="font-small">영문,숫자,특수문자 혼합하여 8자리~20자리 이내</div></td>
			</tr>

			<!-- 비밀번호 확인 -->
			<tr>
				<th class="text-center">PwCheck</th>

				<td class="padding-top-bottom"><div class="ui input">
						<input type="password" placeholder="비밀번호를 동일하게 한번 더 입력해주세요"
							id="pw2" oninput="pwcheck()">
					</div>
					<div id="pw2confirm" class="font-small">동일한 비밀번호를 입력해주세요</div></td>

			</tr>

			<!-- 성별姓別 -->
			<tr>
				<th class="text-center">Gender</th>

				<td class="padding-top-bottom"><div class="ui form">
						<div class="inline fields" style="margin-bottom: 0px;">
							<div class="ui radio checkbox">
								<div class="field">
									<input width="5px" type="radio" value="1" name="sex"><label>남</label>
								</div>
							</div>
							<div class="field">
								<div class="ui radio checkbox">
									<input width="5px" type="radio" value="2" name="sex"><label>여</label>
								</div>
							</div>
						</div>
					</div>
					<div id="genderconfirm" class="font-small">필수사항입니다 선택해주세요</div>
					</td>

			</tr>

			<!-- 誕生日 -->
			<tr>
				<th class="text-center">Birth</th>
				<td class="padding-top-bottom"><div class="ui calendar" id="birth-visible">
						<div class="ui input left icon">
							<i class="calendar icon"></i> <input type="text"
								placeholder="Date">
						</div>
					</div> <input type="hidden" id="birth" name="birth">
					<div id="birthconfirm" class="font-small">양식에 맞게 적어주세요</div>
					</td>

			</tr>

			<!-- E-mail -->
			<tr>
				<th class="text-center">E mail</th>
				<td class="padding-top-bottom"><div class="ui input">
						<input type="text" placeholder="example:kiokiman@kioki.com"
							id="email" name="email" oninput='checkEmail()'>
					</div>
					<div id="emailconfirm" class="font-small">필수입력사항입니다</div>
					</td>

			</tr>
			<!-- 지역 -->
			<tr>
				<th class="text-center">My local</th>
				<td class="padding-top-bottom"><div class="ui selection dropdown" id="local">
						<input name="lno" type="hidden"> <i class="dropdown icon"></i>
						<div class="default text">都道府県</div>

					</div>
					<div class="ui selection dropdown" id="localinner">
						<input name="cno" type="hidden"> <i class="dropdown icon"></i>
						<div class="default text">市区</div>
						<div class="menu" id="selectCenter">
							<div class="item" data-value="1">Loading...</div>
						</div>
					</div>
					<div id="birthconfirm" class="font-small">거주 지역을 골라주세요.</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;">
				<button class="ui primary button" disabled="disabled" onclick="beforeSubmit();return false;"
					id="signupbtn">Sign Up</button>
				<button type="button" class="ui button" id="cancelbtn" onclick="return gohome()">Cancel</button>
				</td>
			</tr>
		</table>
	</div>
	<input type="hidden" name="latitude" id="latitude"/>
	<input type="hidden" name="longitude" id="longitude"/>
</form>
<%@ include file="../inc/footer.jsp" %>