<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>   
<%@ include file="../inc/header.jsp" %>  
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="<c:url value="/resources/jquery/jquery-3.3.1.min.js"/>"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>KIOKI 지금 이순간도 재난 재해가 발생하고 있습니다</title>
<style>
	.centerdiv{
		text-align: center;
	}
	.ui form{
		text-align: center;
	}
	h1{
		color: #ff5722;
	}
</style>
<script type="text/javascript">

	//Accordion
	$(function(){
		 $('#loginbutton').on('click',formCheck);
	});
	
	//로그인 폼 검사
	function formCheck(){
		var id = $('#id').val();
		var password = $('#password').val();
				
		if(id.length == 0){
			alert("ID를 입력해주세요");
			return;
		}
		
		if(password.length == 0){
			alert("PW를 입력해주세요");
			return;
		}
		
		$.ajax({
			url: "login",
			type: "post",
			contentType: "application/json; charset=utf-8",
			data: JSON.stringify({
				username : id,
				pass : password
			}),
			dataType: "text",
			success:function(result){
				if(result=="true"){
					location.href = "../localView";
				}else{
					alert("Not found User Id or Password");
				}
			},
			error:function(e){
				console.log(e);
			}
		});
		
	}
</script>
</head>
<body style="background-size: 100%; background-image:url(${pageContext.request.contextPath}/resources/img/bg.jpg);">
<div class="centerdiv">
	<div class="ui two column middle aligned very relaxed stackable grid" style="margin-top:30px;">
	  <div class="ui circular segment column" style="text-align: center; margin: 0 auto; max-width: 400px;">
	  	<br>
		<h1>KIOKI</h1>
	    <div class="ui form">
	      <div class="field">
	        <label>Username</label>
	        <div class="ui left icon input">
	          <input type="text" id="id" placeholder="Username">
	          <i class="user icon"></i>
	        </div>
	      </div>
	      <div class="field">
	        <label>Password</label>
	        <div class="ui left icon input">
	          <input type="password" id="password">
	          <i class="lock icon"></i>
	        </div>
	      </div>
	      <div class="ui buttons">
			  <button class="ui  positive button" tabindex="0" id="loginbutton">LOGIN</button>
			  <div class="or"></div>
			  <button class="ui negative button" id="join" onclick="location.href='joinForm';">JOIN</button>
		  </div>
	    </div>
	     <br>
	  </div>
	</div>

</div>
</body>
</html>
<%-- <%@ include file="../inc/footer.jsp" %> --%>
