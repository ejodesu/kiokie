<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="inc/header.jsp"%>
<script src="${pageContext.request.contextPath}/resources/maps/markerclusterer.js"></script>
<script>
	var map = null;
	var markers = [];
	var marker = null;
	var infowindow = null;
	$(function() {
		
		$('#localBtn').on('click', goprologue);
		$('#gpsBtn').on('click', currentPos);
		$('.dropdown').dropdown();
		//메뉴버튼 트랜
		$('#weatherBtn').on('click', weatherTran);
		$('#overallBtn').on('click', overallTran);
		$('#disasterBtn').on('click', disasterTran);
		$('#accidentBtn').on('click', accidentTran);
		$('#healthBtn').on('click',healthTran);
		
		$('.poibtn').on('click', searchPOI);

		$('.ui.progress .bar').css('width', '100%');
		$('#progress').html('100 外で遊ぼう');
		$('#progressLeft').html('100 外で遊ぼう');
		
		getAlert("<c:url value='/' />");
	});
	
	//new modal
	function newsmodal(event){
		console.log(event.currentTarget);
		$('#newsDiv').modal('show');
		$('#newsheader').html(event.currentTarget.children[0].innerHTML);
		$('#newscontent').html("<p>"+event.currentTarget.getAttribute("data-summary")+"</p><p><a rel='external' target='_blank' href='"+event.currentTarget.getAttribute("data-link")+"'>Read more...</a>");
	}
	
	// the smooth zoom function
	function smoothZoom (map, max, cnt) {
	    if (cnt > max) {
	        return;
	    }
	    else {
	        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
	            google.maps.event.removeListener(z);
	            smoothZoom(map, max, cnt + 1);
	        });
	        setTimeout(function(){map.setZoom(cnt)}, 80); // 80ms is what I found to work well on my system -- it might not work well on all systems
	    }
	}
	
	// Adds a marker to the map and push to the array.
    function addMarker(loc,icon,label,desc,extra) {
      var marker = new google.maps.Marker({
        position: loc,
        icon: icon,
        /*icon >> number.png*/
        animation: /*google.maps.Animation.BOUNCE*/null,
        /*, map: map*/
        title: label
      });
      marker["desc"]=desc;
      marker["extra"]=extra;
      
      markers.push(marker);
      marker.addListener('click', function(event) {
          //map.setZoom(Math.max(map.getZoom(),15));
          //map.panTo(marker.getPosition());
          //
          //smoothZoom(map, 15, map.getZoom());
          
          //if(idleListener){
        	//  idleListener.remove();
        	//  idleListener=null;
          //}
          infowindow.setContent("<div class='ui grey segment'><h6>"+marker.getTitle()+"</h6><p style='font-size:11pt;'>"
          +marker["desc"]+"</p><p style='font-size:10pt;'>"+marker["extra"]+"</p><button class='ui button' style='font-size:small;' onclick='window.open(\"https://www.google.com/maps/search/?api=1&query="+loc.lat+","+loc.lng+"\")'><i class='google icon'></i>Google map</button></div>");
          infowindow.open(map, marker);
          map.addListener("idle",mapIdle);
        });
      
      if(typeof label == "undefined" || typeof desc == "undefined"){
    	  return;
      }
      
      /*<div class="item">
			  	<img class="ui avatar image" src="${pageContext.request.contextPath}/resources/img/app.png"/>
			    <div class="content">
			      <a class="header">test</a>
			      <div class="description">Updated 10 mins ago</div>
			    </div>
			  </div>*/
      
      if($('#poilist>div').length<6){
    	  if(!(markers.length >= 2 && markers[markers.length-2].lat == loc.lat && markers[markers.length-2].lng == loc.lng)){
	    	  $('#poilist').append("<div class=\"item\">"
	  			  	+"<img class=\"ui avatar image\" src=\""+icon+"\"/>"
				   +"<div class=\"content\" style=\"max-width:80%;\" onclick=\"poiitem_click(this)\" data-lat=\""+loc.lat+"\" data-lng=\""+loc.lng+"\">"
				      +"<a class=\"header\" style=\"white-space:nowrap;text-overflow:ellipsis;overflow:hidden;width:100%;\">"+label+"</a>"
				      +"<div class=\"description\" style=\"white-space:nowrap;text-overflow:ellipsis;overflow:hidden;width:100%;\">"+desc+"</div>"
				    +"</div>"
	  				  +"</div>");
    	  }
      }
    }
	
	var markerClusterer = null;
 	// Sets the map on all markers in the array.
    function setMapOnAll(zoomset) {
 		//맵 클러스터를 통해 다수의 항목을 하나로 모은다.
      /*for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }*/
    	markerClusterer = new MarkerClusterer(map, markers, {
            maxZoom: typeof zoomset == "undefined" ? 12 : zoomset,
            gridSize: null,
            styles: [{
                url: '${pageContext.request.contextPath}/resources/img/clusterer.png',
                height: 48,
                width: 48,
                anchor: [0, 0],
                textColor: '#000000',
                textSize: 20,
                iconAnchor: [15, 48]
              }],
            imagePath: '../images/m'
          });
    }
 	
 	// Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
	   	if (markerClusterer) {
	           markerClusterer.clearMarkers();
	         }
      setMapOnAll();
      $('#poilist').html('');
    }

    // Shows any markers currently in the array.
    function showMarkers() {
      setMapOnAll();
    }

    // Deletes all markers in the array by removing references to them.
    // Overall 등에서 호출된다.
    function deleteMarkers() {
    	if (markerClusterer) {
	           markerClusterer.clearMarkers();
	         }
      markers = [];
      displayingPOI = null;
      $('#poilist').html('');
    }
    
    //마커 배열 순서별로 마커 (쓰이지 않음)
    function addMarkerWithTimeout(position, timeout, icon, animation) {
        window.setTimeout(function() {
          markers.push(new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            label: position["label"],
            animation: animation ? google.maps.Animation.DROP : null
          }));
        }, timeout);
      }

    //markersArray 지우기
      function clearMarkersArray() {
    	  if (markerClusterer) {
	           markerClusterer.clearMarkers();
	         }
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
        markers = [];
        $('#poilist').html('');
      }

	//메뉴버튼클릭 효과
	function overallTran() {
		$('#newslist').css('display','');
		$('#widgetLeft').css('display', 'none');
	    $('#sheltermenu').css('display','none');
	    $('#policemenu').css('display','none');
	    $('#hospitalmenu').css('display','none');
	    $("#widget").animate({left: '0px'});
	    deleteMarkers();
	}
	
	function weatherTran() {
		$('#newslist').css('display','none');
		$('#widgetLeft').css('display', 'inline-block');
		$("#widgetLeft").animate({left: '-400px'});
	    $('#sheltermenu').css('display','none');
	    $('#hospitalmenu').css('display','none');
	    $("#widgetLeft").animate({left: '0px'});
	    //deleteMarkers();
		//setMapOnAll();
	}
	function disasterTran() {
		$('#newslist').css('display','none');
		$('#widgetLeft').css('display', 'none');
	    $('#hospitalmenu').css('display','none');
	    $('#policemenu').css('display','none');
	    deleteMarkers();
	    
	    //서브메뉴 보이기
	    var isnone = $('#sheltermenu').css('display');
		if (isnone == 'none') {
			$('#sheltermenu').css('display','');
		} else {
			$('#sheltermenu').css('display','none');
		}
		
	    var icon = $("#scoreicon").attr("class");
	    $("#scoreicon").removeClass(icon);
	    $("#scoreicon").addClass("fire icon");
	    $("#widgetLeft").animate({left: '0px'});
	    
	}
	function accidentTran() {
		$('#newslist').css('display','none');
		$('#widgetLeft').css('display', 'none');
	    $('#sheltermenu').css('display','none');
	    $('#hospitalmenu').css('display','none');
	    deleteMarkers();
	    
	 	//서브메뉴 보이기
	    var isnone = $('#policemenu').css('display');
		if (isnone == 'none') {
			$('#policemenu').css('display','');
		} else {
			$('#policemenu').css('display','none');
		}
		
		$("#widgetLeft").animate({left: '0px'});
	}
	//병원메뉴
	function healthTran() {
		$('#newslist').css('display','none');
		$('#widgetLeft').css('display', 'none');
	    $('#sheltermenu').css('display','none');
	    $('#policemenu').css('display','none');
	    deleteMarkers();
	    
	    //서브메뉴 보이기
	    var isnone = $('#hospitalmenu').css('display');
		if (isnone == 'none') {
			$('#hospitalmenu').css('display','');
		} else {
			$('#hospitalmenu').css('display','none');
		}
		
		$("#widgetLeft").animate({left: '0px'});
	    
	    deleteMarkers();
	}
/**/	
	var displayingPOI = null;
	function searchPOI(event){
		var lat = map.getCenter().lat();
		var lng = map.getCenter().lng();
		/*whoami 어떤 버튼이 눌렸는지 판단하는 */
		var whoami = null;
		if(event == null){
			whoami = displayingPOI;
		} else {
			whoami = event.target.id;
		}
		
		if(whoami == null){
			return; //보일 POI 종류가 없음
		}
		displayingPOI = whoami;
		
		var type = 0;
		var sheltertype = 0;
		var icon = "";
		var cat=null;
		
		switch(whoami){
		case "policeBtn":
			type = 1;
			icon = '${pageContext.request.contextPath}/resources/img/police.png';
			cat="Police Station";
			break;
		case "fireBtn":
			type = 2;
			icon = '${pageContext.request.contextPath}/resources/img/fireman.png';
			cat="Fire station";
			break;
		case "tempBtn":
			type = 3;
			sheltertype = 1;
			icon = '${pageContext.request.contextPath}/resources/img/temp.png';
			cat="Emergency Temporary Shelter";
			break;
		case "appBtn":
			type = 3;
			sheltertype = 2;
			icon = '${pageContext.request.contextPath}/resources/img/app.png';
			cat="Major Shelter";
			break;
		case "waBtn":
			type = 3;
			sheltertype = 3;
			icon = '${pageContext.request.contextPath}/resources/img/widearea.png';
			cat="Widearea Shelter";
			break;
		case "hospitalBtn":
			type = 4;
			icon = '${pageContext.request.contextPath}/resources/img/hospital.png';
			cat="Emergency Network Hospital";
			break;
		case "weatherBtn":
			type = 5;
			getWeather();
			return;
		case "healthBtn":
			type = 6;
			getWeather(true);
			return;
		}
		
		$.ajax({			
			url: "searchRect",
			type: "get",
			data: { //double lat, double lng, int type, 
				//double southlat, double northlat, double westlng, double eastlng, Optional<Integer> shelterType
				lat : lat,
				lng : lng,
				type : type,
				sheltertype: sheltertype,
				southlat : map.getBounds().getSouthWest().lat(),
				northlat : map.getBounds().getNorthEast().lat(),
				westlng : map.getBounds().getSouthWest().lng(),
				eastlng : map.getBounds().getNorthEast().lng()
			},
			dataType : 'JSON',
			success:function(data){
				console.log(data);
				clearMarkersArray();
				
				if(event != null && (data == null || data.length == 0)){
					$.uiAlert({
						textHead: 'No places found', // header
						text: 'Please zoom out or move to proper location.', // Text
						bgcolor: '#55a9ee', // background-color
						textcolor: '#fff', // color
						position: 'bottom-left',// position . top And bottom ||  left / center / right
						icon: 'info circle', // icon in semantic-UI
						time: 3, // time
						  });
				}
				
				for (var i = 0; i < data.length; i++) {
		          /*addMarkerWithTimeout(data[i], event != null ? i * 20 : 0, icon,
		        		  event != null);*/
					var extra = null;
					if(data[i].hasOwnProperty("disasterType")&&typeof data[i]["disasterType"] == "number"&&data[i]["disterType"]>0){
						var dtype = "This shelter is prepared for: ";
						if((data[i]["disasterType"] & 1) == 1){ dtype = null;}
						if((data[i]["disasterType"] & 32) == 32){ dtype += "Earthquake ";}
						if((data[i]["disasterType"] & 16) == 16){ dtype += "Tsunami ";}
						if((data[i]["disasterType"] & 8) == 8){ dtype += "Typhoon ";}
						if((data[i]["disasterType"] & 4) == 4){ dtype += "Volcanic ";}
						if((data[i]["disasterType"] & 2) == 2){ dtype += "Others ";}
						extra = dtype;
						extra = extra == null ? extra : extra + "<br/>";
					}
					if(data[i].hasOwnProperty("fscale")&&data[i]["fscale"]!=null&&data[i]["fscale"]>0){
						var fscale = "Size: "+data[i]["fscale"]+"㎡<br/>";
						extra = extra == null ? fscale : extra + fscale;
					}
					if(data[i].hasOwnProperty("seatingcap")&&data[i]["seatingcap"]!=null&&data[i]["seatingcap"]>0){
						var cap = "Capacity: "+data[i]["seatingcap"]+" people<br/>";
						extra = extra == null ? cap : extra + cap;
					}
					if(data[i].hasOwnProperty("subjects")&&data[i]["subjects"]!=null){
						extra = data[i]["subjects"];
					}
		        	addMarker(data[i],icon,data[i]["bname"],data[i]["desc"],"<b>"+cat+"</b>"+(extra!=null?"<br/>"+extra:""));
		        	
		        }
		        setMapOnAll();
			},
			error:function(e){
				if(event != null){
					$.uiAlert({
						textHead: 'Unknown error from server', // header
						text: 'Please try again later.', // Text
						bgcolor: '#55a9ee', // background-color
						textcolor: '#fff', // color
						position: 'bottom-left',// position . top And bottom ||  left / center / right
						icon: 'warning sign', // icon in semantic-UI
						time: 2, // time
						  });
				}
				console.log(e);
			}
		});
	}

	/*날씨정보 불러오기*/
	/*  weatherBtn */
	 function getWeather(ispm25){	
			 
				$.ajax({
					url : "searchWeather",
					type : "get",				
					dataType : 'JSON',
					success : function(data){
						clearMarkersArray();
						var weatherlist = data;
						
						$.each(weatherlist, function (key, value){
					    	console.log(value);
					    		/*해당 지역에 날씨 정보 마커 */
					        	var marker = new google.maps.Marker({
					          		position: {lat:value.lat, lng:value.lng},
					          		icon: ispm25 ? '${pageContext.request.contextPath}/resources/img/weather/pm25/pm25_'+value.pm25+'.png' :
					          				'${pageContext.request.contextPath}/resources/img/weather/'+value.weather+'.png',
					         	 	/*icon >> number.png*/
					          		animation: null,
					          		title: ispm25? String(value.pm25):value.weatherdesc
					        	});				        
					    	
					    	
					        marker["info"]=value;
					        
					        markers.push(marker);
					        //marker.setMap(map);
					        
					        marker.addListener('click', function(event) {
					            //map.setZoom(Math.max(map.getZoom(),15));
					            //map.panTo(marker.getPosition());
					            //
					            //smoothZoom(map, 15, map.getZoom());
					            
					            //if(idleListener){
					          	//  idleListener.remove();
					          	//  idleListener=null;
					            //}
					            infowindow.setContent(
					            		"<table class='ui green small table'>"
					            		  +"<thead><tr><th colspan='8'>"+value.lsname+"</th>"
					            		    +"</tr></thead><tbody>"
					            		    +"<tr>"
					            		    +"<td>PM2.5</td>"
					            		    +"<td"+(value.pm25 == 1 ? " style='color: white; background-color:grey;'":"")+">1</td>"
					            		    +"<td"+(value.pm25 == 2 ? " style='color: white; background-color:#2185d0;'":"")+">2</td>"
					            		    +"<td"+(value.pm25 == 3 ? " style='color: white; background-color:#00b5ad;'":"")+">3</td>"
					            		    +"<td"+(value.pm25 == 4 ? " style='color: white; background-color:#b5cc18;'":"")+">4</td>"
					            		    +"<td"+(value.pm25 == 5 ? " style='color: white; background-color:#fbbd08;'":"")+">5</td>"
					            		    +"<td"+(value.pm25 == 6 ? " style='color: white; background-color:#f2711c;'":"")+">6</td>"
					            		    +"<td"+(value.pm25 == 7 ? " style='color: white; background-color:#db2828;'":"")+">7</td>"
					            		    +"</tr>"
					            		    +"<tr><td colspan='8'>(0から７までの数値です（0が一番養護）)</td></tr>"
					            		    +"<tr>"
					            		    +"<td style='color: red;'>最高気温</td>"
					            		    +"<td colspan='7'style='color: red;'>"+value.max +"</td>"
					            		    +"</tr>"
					            		    +"<tr>"
					            		    +"<td style='color: blue;'>最低気温</td>"
					            		    +"<td colspan='7' style='color: blue;'>"+value.min +"</td>"
					            		    +"</tr>"
					            		    +"<tr>"
					            		    +"<td style='color: red;'>注意</td>"
					            		    +"<td colspan='7' style='color: red;'>"+(value.alert||'特になし') +"</td>"
					            		    +"</tr>"
					            		    +"<tr>"
					            		    +"<td colspan='8'>"+value.updated+"</td>"
					            		    +"</tr>"
					            		    +"<tr>"
					            		    +"<td>情報</td>"
					            		    +"<td colspan='7'><a href="+value.url +" target='_blank'>tenki.jpへ</a></td>"
					            		    +"</tr>"
					            		    +"</tbody>"
					            		    +"</table>"		
					            );
					            /* infowindow.setContent("<div class='ui olive segment'><div class='ui olive label'>"+value.lsname+"</div>"
					            +"<div class='ui label'>"+marker.getTitle()+"</div>"
					            +"<div class='ui label'>pm2.5："+value.pm25+"</div>"
					            +"<div class='ui label'>(0から７までの数値です（0が一番養護）)</div>"
					            +"<div class='ui red label'>最高気温："+value.max +"</div>"
					            +"<div class='ui blue label'>最低気温："+value.min +"</div>"
					            +"<div class='ui red label'>注意："+(value.alert||'特になし') +"</div>"
					            +"<p style='font-size:small'>"+value.updated+"</p>"
					            +"<div class='ui label'>情報：<a href="+value.url +" target='_blank'>tenki.jpへ</a></div></div>"); */
					            infowindow.open(map, marker);
					            map.addListener("idle",mapIdle);
					          });	/*listener*/
						});/*for each*/		
					    setMapOnAll(1);
					}
					
				});	/*ajax end*/				
				
		
					
	}/*function getWeather end*/

	//local side bar
	function goprologue() {
		location.href = "${pageContext.request.contextPath}/prologue";
	}
	// Accordion
	function myFunction(id) {
		var x = document.getElementById(id);
		if (x.className.indexOf("w3-show") == -1) {
			x.className += " w3-show";
			x.previousElementSibling.className += " w3-theme-d1";
		} else {
			x.className = x.className.replace("w3-show", "");
			x.previousElementSibling.className = x.previousElementSibling.className
					.replace(" w3-theme-d1", "");
		}
	}

	// Used to toggle the menu on smaller screens when clicking on the menu button
	function openNav() {
		var x = document.getElementById("navDemo");
		if (x.className.indexOf("w3-show") == -1) {
			x.className += " w3-show";
		} else {
			x.className = x.className.replace(" w3-show", "");
		}
	}
	var idleListener = null;
	var idletimer = null;
	var geocoder;
	function initMap() {
		geocoder = new google.maps.Geocoder();
		
		var isDefaultLocation = false;
		<c:choose>
		<c:when test="${not empty lat}">
		var mlat = ${lat};
		var mlng = ${lng};
		</c:when>
		<c:otherwise>
		var mlat = $('#slat').val();
		var mlng = $('#slng').val();
		if (typeof mlng == "undefined" || mlng.length == 0) {
			mlat = $('#lat').val();
			mlng = $('#lng').val();
			if (typeof mlng == "undefined" || mlng.length == 0) {
				/* tokyo lng lat */
				mlng = 139.69167;
				mlat = 35.68944;
				isDefaultLocation = true;
			}
		}
		</c:otherwise>
		</c:choose>
		
		var initialZoom = 15;
		<c:if test="${not empty zoom}">
		initialZoom = ${zoom};
		</c:if>
		
		console.log(mlat);
		console.log(mlng);
		var loc = {
			lat : parseFloat(mlat),
			lng : parseFloat(mlng)
		};

		var gestureOption = window.matchMedia("(max-width: 700px)").matches ?
				'cooperative' : 'greedy'; //창 크기에 따라 생성 단계에서 Ctrl 스크롤 여부 결정
		map = new google.maps.Map(document.getElementById('map'), {
			zoom : initialZoom,
			gestureHandling: gestureOption,
			center : loc
		});
				
		idleListener=map.addListener("idle", mapIdle); //end addListener


		//검색된 지역의 빨간 네모
		var boundingBox = null;
		<c:if test="${searchloc != null}">
		var address = "${searchloc.lname}"; //매개변수에 의한 탐색을 확인
	    geocoder.geocode( { 'address': address}, function(results, status) {
	      if (status == 'OK') {
	    	  var ne = results[0].geometry.viewport.getNorthEast();
	          var sw = results[0].geometry.viewport.getSouthWest();

	          map.fitBounds(results[0].geometry.viewport);               

	          var boundingBoxPoints = [
	             ne, new google.maps.LatLng(ne.lat(), sw.lng()),
	             sw, new google.maps.LatLng(sw.lat(), ne.lng()), ne
	          ];

	          boundingBox = new google.maps.Polyline({
	             path: boundingBoxPoints,
	             strokeColor: '#FF0000',
	             strokeOpacity: 1.0,
	             strokeWeight: 2
	          });

	          //boundingBox.setMap(map);
	      } else {
	        console.log('Geocode was not successful for the following reason: ' + status);
	      }
	    });
	    </c:if>
	    if(isDefaultLocation){
	    	currentPos();
	    }
	    
	    infowindow = new google.maps.InfoWindow({
	        content: null,
	        maxWidth: 250
	      });
	    onLocationChange($('#placeName').text().trim());	    
	}
	function mapIdle() {
		if(idletimer){
			clearTimeout(idletimer);
			idletimer=null;
		}
		idletimer=setTimeout(function(){
		    var latitude = map.getCenter().lat();
		    var longitude = map.getCenter().lng();
		    var czoom = map.getZoom();
		    //console.log( latitude + ', ' + longitude );
		    history.replaceState(null, "local", "${pageContext.request.contextPath}/localView/@"+latitude+","+longitude+","+czoom+"z");
		    
		    searchPOI(null);
		    //administrative_area_level_1 locality
		    geocoder.geocode({'location': map.getCenter(), 'region': 'ja'}, function(results, status){
		    	if (status === 'OK') {
		    		var txt = null;
		    		if(results!=null && results.length > 0){
			    		results[0]["address_components"].forEach(function(a){
			    			if(a["types"].indexOf("administrative_area_level_1")>=0){
			    				txt=a;
			    			}
			    		});
		    		}
		    		
		    	      if (txt) {
		    	    	  var e = txt;
		    	    	  //ad.forEach(function(e){
		    	    		 //if(e["types"].indexOf("administrative_area_level_1")>=0){
		    	    			 if($("#placeName").text() != e["short_name"]){
		    	    				 //다른 장소로 변경됨
		    	    				 $("#placeName").text(e["short_name"]); 
		    	    				 onLocationChange(e["short_name"]);
		    	    			 }
		    	    		 //} 
		    	    	  //});
		    	      } else {
		    	        console.log('No results found');
		    	      }
		    	    } else {
		    	    	console.log('Geocoder failed due to: ' + status);
		    	    }
		    });
		},500);
	}
	function currentPos(){
		if ("geolocation" in navigator) {
			  /* geolocation is available */
				navigator.geolocation.getCurrentPosition(function(position) {
					var center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				    // using global variable:
				    map.setCenter(center);
					limitDrag();
					});
			} else {
			  /* geolocation IS NOT available */
			}
	}
	//지도의 드래그를 제한하고 일본을 중앙으로 한다.
    function limitDrag()
	    {
  	  	var MapBounds = new google.maps.LatLngBounds(
	  			    new google.maps.LatLng(24.08315134683273, 122.88657950891195),
	  			    new google.maps.LatLng(45.50579721305191, 145.94566460207386));
	        if (MapBounds.contains(map.getCenter()))
	        {
	            return;
	        }
	        else
	        {
	            map.setCenter(new google.maps.LatLng(mlat,mlng));
	        }
	    }

	//poilist의 항목을 누르면 지도를 그 위치로 이동한다.
	function poiitem_click(sender)
	{
		if(sender != null){
			var tlat = parseFloat($(sender).attr("data-lat"));
			var tlng = parseFloat($(sender).attr("data-lng"));
			
			map.setZoom(Math.max(15,map.getZoom()));
			map.panTo({lat:tlat, lng:tlng});
		}
	}
	
	
	
	function onLocationChange(title){
		//지역이 변경된 경우 발생하는 이벤트 (좌측 패널 업데이트에 활용)		
		console.log("lchanged: "+title);
		$.ajax({
			url : '${pageContext.request.contextPath}/news/list',
			type : 'get',
			data : {title:title},
			contentType: 'application/JSON; charset=utf-8',
			success: function (data) {
				var newslist = data;

				var news = '';

				$.each(newslist, function (key, value) {
										
					news +='<a class="item" data-summary="'+value.content+'" data-posted="'+value.posted+'" data-link="'+value.link+'">';
     				news +='<h6 style="font-size:100%; font-weight: bold;">'+value.title+'</h6>';
     			
     			if(key < 1){
   					news +='<p style="font-size:80%;">'+value.content+'</p>';
   					news +='<p style="font-size:70%;">'+value.posted+'</p>';
   					 					
     				}
     				news +='</a>';  
   				$('#news').html(news);
   				$('#news a').on('click', newsmodal);
				});
				
				}
		});
		
		$.ajax({
			url : '${pageContext.request.contextPath}/weatherscore',
			type : 'get',
			data : {title:title},
			success: function (data) {
					var num = Number(data);
					console.log(num);
					$('#leftProgressDiv').progress({
						percent: num
					});
					switch(true){
					case (num < 16.6):
						$(".statistic img").attr("src","${pageContext.request.contextPath}/resources/img/score/s1.png");
						$("#leftProgressDiv").attr('class','ui active red progress');
						$("#scorelabel").attr('class','ui red label');
						$("#scorelabel").html("布団の外は危険です");
						break;
					case (num < 33.3):
						$(".statistic img").attr("src","${pageContext.request.contextPath}/resources/img/score/s2.png");
						$("#leftProgressDiv").attr('class','ui active orange progress');
						$("#scorelabel").attr('class','ui orange label');
						$("#scorelabel").html("家がよくないですか");
						break;
					case (num < 50):
						$(".statistic img").attr("src","${pageContext.request.contextPath}/resources/img/score/s3.png");
						$("#leftProgressDiv").attr('class','ui active yellow progress');
						$("#scorelabel").attr('class','ui yellow label');
						$("#scorelabel").html("外出も悪くはないです");
						break;
					case (num < 66.6):
						$(".statistic img").attr("src","${pageContext.request.contextPath}/resources/img/score/s4.png");
						$("#leftProgressDiv").attr('class','ui active blue progress');
						$("#scorelabel").attr('class','ui blue label');
						$("#scorelabel").html("約束を取ってもいいです");
						break;
					case (num < 83.3):
						$(".statistic img").attr("src","${pageContext.request.contextPath}/resources/img/score/s5.png");
						$("#leftProgressDiv").attr('class','ui active olive progress');
						$("#scorelabel").attr('class','ui olive label');
						$("#scorelabel").html("今日は運動をしてみましょう");
						break;
					default:
						$(".statistic img").attr("src","${pageContext.request.contextPath}/resources/img/score/s6.png");
						$("#leftProgressDiv").attr('class','ui active green progress');
						$("#scorelabel").attr('class','ui green label');
						$("#scorelabel").html("外で楽しく遊ぼ");
						break;
					}				
				}
		});
		
	}
	
	

	
	
</script>
<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClDCEjwzxMODU_oPzJwX3g0554e-SXB0A&callback=initMap&language=ja&region=JP">
</script>
<style>
	#dislikeBtn{
		cursor: pointer;
		transition: 0.5s;
	}
	#dislikeBtn:hover{
		color: red;
	}
	#usefulBtn{
		cursor: pointer;
		transition: 0.5s;
	}
	#usefulBtn:hover{
		color: green;
		
	}
</style>
<!-- The Grid -->
<div class="w3-row mapcontainer">
	<!-- Left Column -->
	<div class="w3-col m3">
		<br>
		<!-- 위치 -->
			<div class="ui orange fluid buttons">
			  <div class="ui button fluid" id="gpsBtn"><i class="location arrow icon"></i><div id="placeName" style="clear:none;display:inline;">
	        		<c:choose>
						<c:when test="${searchloc.getLname() !=null}">
						${searchloc.getLname()}
	        			</c:when>
						<c:otherwise>
						<c:choose>
							<c:when test="${sessionScope.loginlocal.getLname()!=null}">
								${sessionScope.loginlocal.getLname()}
	        				</c:when>
							<c:otherwise>
								東京
	        				</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose></div></div>
			  <div class="ui floating pointing dropdown icon button" style="margin-left:-30px;">
			    <i class="dropdown icon"></i>
			    <div class="menu">
			      <div class="item" id="localBtn"><i class="sync alternate icon"></i>Change location...</div>
			    </div>
			  </div>
			</div>
			
		<div class="ui left demo vertical inverted sidebar labeled icon menu"
			id="sidemenu">
			<a class="item"> Home </a> <a class="item"> Topics </a> <a
				class="item"> Friends </a>
		</div>

		<!-- Alert Box -->
		<div class="ui red segment w3-animate-left" style="border-color: #d32f2f;" id="alertbox" style="display:none;">
			<div class="ui red ribbon label" style="font-size:15px;">
        		<i class="exclamation circle icon"></i> お知らせ
      		</div>
			<p>TEXT</p>
		</div>
		
		<!-- Score Box Left-->
		<div class="ui blue segment "
			style="display: none; width: 100%; margin-top: 10px;" id="widgetLeft">

				<div class="ui blue ribbon label">
        			<i class="chart bar icon"></i> Score
      			</div>
			      	<div class="statistic w3-center" style="margin-top: 10px;">
			      		<div id="scoreLeft">
							<img src="${pageContext.request.contextPath}/resources/img/score/s5.png" style="color: blue; height: 128px;">
						</div>
					  <div class="ui label" id="scorelabel"></div>
					</div>
					<div class="ui active progress" id="leftProgressDiv" style="margin: 15px;">
					  <div class="bar" >
					  	<div class="progress" id="progressLeft"></div>
					  </div>
					</div>

		</div>

		<!-- NEWS -->
		<div class="ui bottom attached active tab brown segment w3-animate-left" id="newslist" style="margin-top: 10px;">
			<div class="ui black ribbon label">
		        <i class="newspaper outline icon"></i> News
		      </div>
			<div class="ui vertical menu" style=" width: 100%;" id="news">			  
			</div>
		</div>
		<br>
		<br>

		<!-- End Left Column -->
	</div>

	<!-- Middle Column -->
	<div class="w3-col m7 mapcontainer">

		<div id="map" class="w3-container w3-card w3-white w3-round w3-margin">

			<br>

		</div>

	</div>

	<!-- Right Column -->
	<div class="w3-col m2">
		<br>
		<!-- menu -->
		<div class="w3-card w3-round">
			<div class="w3-white">
				<button id="overallBtn"
					class="w3-button w3-block w3-theme-l1 w3-left-align w3-hover-deep-orange">
					<i class="dot circle icon"></i> News
				</button>
				<button id="weatherBtn" 
					class="w3-button w3-block w3-theme-l1 w3-left-align w3-hover-deep-orange poibtn">
					<i class="sun icon"></i> Weather
				</button>
				<button id="disasterBtn"
					class="w3-button w3-block w3-theme-l1 w3-left-align w3-hover-deep-orange">
					<i class="fire icon"></i> Disaster
				</button>
				<button id="accidentBtn"
					class="w3-button w3-block w3-theme-l1 w3-left-align w3-hover-deep-orange">
					<i class="exclamation triangle icon"></i> Accident
				</button>
				<button id="healthBtn"
					class="w3-button w3-block w3-theme-l1 w3-left-align w3-hover-deep-orange poibtn">
					<i class="heartbeat icon"></i> Health
				</button>
			</div>
		</div>

		<!-- shelter menu -->
		  <div class="ui raised link vertical pointing menu" id="sheltermenu" style="display: none;">
			  <div class="item">
			    <b>Shelter</b>
				    <div class="menu">
				      <a class="item poibtn" id="tempBtn">Temp Shelter</a>
				      <a class="item poibtn" id="appBtn">Appoint Shelter</a>
				      <a class="item poibtn" id="waBtn">Wide Area Shelter</a>

				    </div>
			  </div>
		  </div>
		  
		 <!-- police&firestation menu -->
		  <div class="ui raised link vertical pointing menu" id="policemenu" style="display: none;">
			  <div class="item">
			    <b>Police & Fire Station</b>
				    <div class="menu">
				      <a class="item poibtn" id="policeBtn">Police</a>
				      <a class="item poibtn" id="fireBtn">Fire Station</a>
				    </div>
			  </div>
		  </div>
		  
		  <!-- hospital menu -->
		  <div class="ui raised link vertical pointing menu" id="hospitalmenu" style="display: none;">
			  <div class="item">
			    <b>Hospital</b>
				    <div class="menu">
				      <a class="item poibtn" id="hospitalBtn">Hospital</a>
				    </div>
			  </div>
		  </div>
		  
		  <!-- poi list menu -->
		  <div class="ui relaxed divided selection list" id="poilist">
		  </div>
		  
		<!-- End Right Column -->
	</div>

	<!-- End Grid -->
</div>
<input type="hidden" id="slng" value="${searchloc.getLongitude()}">
<input type="hidden" id="slat" value="${searchloc.getLatitude()}">
<input type="hidden" id="lat" value="${sessionScope.loginLat}">
<input type="hidden" id="lng" value="${sessionScope.loginLng}">
	
	<!-- news modal -->
	<div id="newsDiv" class="ui longer modal">
	  <div class="header" id="newsheader">Header</div>
	  <div class="scrolling content" id="newscontent">
	  	
	  </div>
	</div>
	
<%@ include file="inc/footer.jsp"%>

