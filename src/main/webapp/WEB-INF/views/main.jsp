<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="inc/header.jsp" %>
<script>
function isVideo(name){
	return name.endsWith(".mp4") || name.endsWith(".avi");
}

var map = null;
var mlat, mlng;
	$(function(){
		$('#guide1').on('click',guide1);
		$('#guide2').on('click',guide2);
		$('#guide3').on('click',guide3);
		$('#guide4').on('click',guide4);
		$('#localBtn').on('click', goprologue);
		$('#gpsBtn').on('click',function(){
			location.href="localView";
		});
		$('.dropdown').dropdown();
		getAlert("<c:url value='/' />");
	});
	
		//local side bar
			function goprologue(){
				location.href = "${pageContext.request.contextPath}/prologue";
			}

		//guide1
			function guide1(){
				$('#guideDiv')
				  .modal('setting', 'transition', 'horizontal flip')
        		  .modal('show');
			}
		
		//guide2
			function guide2(){
				$('#guide2Div')
					.modal('setting', 'transition', 'horizontal flip')
      		  		.modal('show');
			}
		//guide3
			function guide3(){
				$('#guide3Div')
				.modal('setting', 'transition', 'horizontal flip')
  		  		.modal('show');
			}
		//guide4
			function guide4(){
				$('#guide4Div')
					.modal('setting', 'transition', 'horizontal flip')
  		  			.modal('show');
			}
		
		 // Accordion
		 	function myFunction(id) {
		 	    var x = document.getElementById(id);
		 	    if (x.className.indexOf("w3-show") == -1) {
		 	        x.className += " w3-show";
		 	        x.previousElementSibling.className += " w3-theme-d1";
		 	    } else { 
		 	        x.className = x.className.replace("w3-show", "");
		 	        x.previousElementSibling.className = 
		 	        x.previousElementSibling.className.replace(" w3-theme-d1", "");
		 	    }
		 	}
		
		 	// Used to toggle the menu on smaller screens when clicking on the menu button
		 	function openNav() {
		 	    var x = document.getElementById("navDemo");
		 	    if (x.className.indexOf("w3-show") == -1) {
		 	        x.className += " w3-show";
		 	    } else { 
		 	        x.className = x.className.replace(" w3-show", "");
		 	    }
		 	}
		 	
	      function initMap() {
	    	  mlat = $('#lat').val();
		      mlng = $('#lng').val();

	    	 if(mlng.length == 0){
	    		 mlng = 139.69167;
	    		 mlat = 35.68944;
	    	 }
	    	 console.log(mlat);
	    	 console.log(mlng);
	    	 var loc = {lat: parseFloat(mlat), lng: parseFloat(mlng)};
	   	   
	  	     map = new google.maps.Map(document.getElementById('map'), {
	  	      minZoom: 6,
	  	      maxZoom: 6,
	  	      zoom: 6,
	  	      center: loc,
	  	    	disableDefaultUI: true
	  	    });
	  	    /* map.event.add
	  	      var marker = new google.maps.Marker({
	  	      position: loc,
	  	      map: map
	  	    });*/
	  	   

	  	  map.addListener('dragstart', limitDrag);
	  			    map.addListener('drag', limitDrag);
	  			  map.addListener('dragend', limitDrag);
	      }	
	      
	      //지도의 드래그를 제한하고 일본을 중앙으로 한다.
	      function limitDrag()
		    {
	    	  var MapBounds = new google.maps.LatLngBounds(
		  			    new google.maps.LatLng(24.08315134683273, 122.88657950891195),
		  			    new google.maps.LatLng(45.50579721305191, 145.94566460207386));
		        if (MapBounds.contains(map.getCenter()))
		        {
		            return;
		        }
		        else
		        {
		            map.setCenter(new google.maps.LatLng(mlat,mlng));
		        }
		    }
	 /*ready function*/    
	 $(document).ready(function(){
		 
	    	  readlivefeed();
	    	  
	    	  getTwitter();
	    	  
	 });      
	      
	/*메인 화면 우측 column livefeed 표시하기*/
	function readlivefeed() {

		$.ajax({
			url: 'livefeed/getlivefeed',
			type: 'GET',
			contentType: 'application/JSON; charset=utf-8',
			success: function (data) {

				var livefeedlist = data;

				var livefeed = '';

				$.each(livefeedlist, function (key, value) {
					/*no라이브피드번호 usernum유저아이디 lno 로컬 cno 도시 content 라이브피드내용
					posted 글 쓴 내용 attachtype 첨부파일 양식 attach 첨부파일*/
					if(key<5){
					livefeed += '<div class="ui vertical segment feed">';
					livefeed += '<div class="ui top attached label" style="background-color:#';
					livefeed += value.color
					livefeed +=';color:white;font-size:small;text-align:center;display:inline;">' +value.lname + ' ' + value.clname + '</div>';
					livefeed += '<div class="content card" style="width:400xp; text-align:center;">';
					livefeed += '<div class="summary">';
					livefeed += '<div class="date" style="font-size:small; color:grey;">';					
					livefeed += ' '+ value.posted + ''; //시간
					livefeed += '</div>';
					livefeed += '<a class="user" style="font-size:small; font-weight: bold;">';
					livefeed += '' + value.username + ''; /*username*/
					livefeed += '</a><br>';
					livefeed += '<a class="user" style="font-size:15pt; font-weight: bold;">';
					livefeed += '</a>' + value.content + ''; /*내용*/
					livefeed += '<div style="margin-bottom: 10px;">';
					/*업로드된 이미지 표시*/
					var img = value.attach;
					if(img !=null){
					var imgsplit = img.split(';');			
					  	for ( var i in  imgsplit ) {
							livefeed += '<'+(isVideo(imgsplit[i])?'video muted autoplay' : 'img')+' src="${pageContext.request.contextPath}/livefeed/files/'+imgsplit[i]+'" style="display:inline;clear:none;max-width:70px;" onclick="this.paused?this.play():this.pause();">'+(isVideo(imgsplit[i]) ? '</video>' : '');   
					  	}
					}
					livefeed += '</div>';
					livefeed += '</div>';
					livefeed += '</div>';
					livefeed += '</div>'; //지역
					}
					//innerHTML					
					$('#livefeed').html(livefeed);
				});	
			   
				
			} //success		

		}); //ajax

	}	//readlivefeed;
	
	
	/*메인 화면 좌측 column twitter 표시하기*/  
	function getTwitter(){
		
		$.ajax({
			url : 'kioki/getTwitter',
			type : 'GET',
			contentType : 'application/JSON; charset=utf-8',
			success: function (data) {
				
				function autolink(doc) {
			        var regURL = new RegExp("(http|https|ftp|telnet|news|irc)://([-/.a-zA-Z0-9_~#%$?&=:200-377()]+)","gi");
			        var regEmail = new RegExp("([xA1-xFEa-z0-9_-]+@[xA1-xFEa-z0-9-]+\.[a-z0-9-]+)","gi");
			        return doc.replace(regURL,"<a href='$1://$2' target='_blank'>$1://$2</a>").replace(regEmail,"<a href='mailto:$1'>$1</a>");
				}
				
				var twitterlist = data;

				var twitter = '';
			
				$.each(twitterlist, function (key, value) {
					twitter += '<div class ="twitter" style="maxHeight:300px;maxWidth:300px">';
					twitter += '<a class="item">';
					twitter +='<p><img style="height:48px;width:48px" src="'+value.avatar+'"style="font-weight:bold;" onerror="this.src=\'<c:url value="/resources/img/anonymous_user_profile.jpg"/>\';">'+value.author+'</p>';
					/* src="<c:url value="/resources/image/banner/b1.jpg"/>" */
					twitter +='<p style="font-size: small;">'+autolink(value.content)+'</p>';
					twitter +='<p style="font-size: small; color: grey;">'+value.posted+'</p>';
					twitter += '</div>';
			
				});

				//innerHTML					
				$('#twitter').html(twitter);			
			}		
		});	
	}
	
	
	

	      
	</script>
	<script async defer
    	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClDCEjwzxMODU_oPzJwX3g0554e-SXB0A&callback=initMap">
    </script>
    <style>
    </style>
  <!-- The Grid -->
  	<div class="w3-row mapcontainer">
    <!-- Left Column -->
    <div class="w3-col m3">                             
    	<br>
    	<!-- 위치 -->
			<div class="ui orange fluid buttons">
			  <div class="ui button fluid" id="gpsBtn"><i class="location arrow icon"></i>
	        			Japan
	        </div>
			  <div class="ui floating pointing dropdown icon button" style="margin-left:-30px;">
			    <i class="dropdown icon"></i>
			    <div class="menu">
			      <div class="item" id="localBtn"><i class="sync alternate icon"></i>Change location...</div>
			    </div>
			  </div>
			</div>
	        
	        <div class="ui left demo vertical inverted sidebar labeled icon menu" id="sidemenu">
			  <a class="item">
			    Home
			  </a>
			  <a class="item">
			    Topics
			  </a>
			  <a class="item">
			    Friends
			  </a>
			</div>
			
		<!-- Alert Box -->
		<div class="ui segment w3-animate-left" style="border-color: #d32f2f;" id="alertbox">
			<div class="ui red ribbon label" style="font-size:15px;">
        		<i class="exclamation circle icon"></i> お知らせ
      		</div>
			<p>TEXT</p>
		</div>
		
		<!-- Twitter 표시 -->
		<div class="ui bottom attached active tab blue segment w3-animate-left" style="margin-top: 10px;">
			
			<div class="ui blue ribbon label">
		        <i class="twitter icon"></i> Twitter
		      </div>
			
			<div class="ui vertical menu" style=" width: 100%;" id="twitter">
			  
			</div>
		</div>
		<br>
		<br>
          
    <!-- End Left Column -->
    </div>
    
    <!-- Middle Column -->
    <div  class="w3-col m7 mapcontainer">
    
    	<div id="map"class="w3-container w3-card w3-white w3-round w3-margin">
	      
	      <br>
      
      </div>
          
    </div>
    
    <!-- Right Column -->
    <div class="w3-col m2">
    	<br>
    	<!-- menu -->
	      <div class="w3-card w3-round">
	        <div class="w3-white">
	          <button id="guide1" class="w3-button w3-block w3-theme-l1-navy w3-left-align w3-hover-blue-grey"><i class="question circle icon"></i> KIOKI 紹介</button>
	          <button id="guide2" class="w3-button w3-block w3-theme-l1-navy w3-left-align w3-hover-blue-grey"><i class="question circle icon"></i> メイン画面ガイド</button>
	          <button id="guide3" class="w3-button w3-block w3-theme-l1-navy w3-left-align w3-hover-blue-grey"><i class="question circle icon"></i> 地域ページ</button>
	          <button id="guide4" class="w3-button w3-block w3-theme-l1-navy w3-left-align w3-hover-blue-grey"><i class="question circle icon"></i> ライブフィード</button>
	        </div>      
	      </div>
       <br>
       
       	<!-- livefeed 표시 -->
		<div class="ui bottom attached active tab teal segment w3-animate-right">
			
			<div class="ui teal ribbon label">
		        <i class="newspaper outline icon"></i> livefeed
		     </div>
		      
			<div class="ui vertical menu" style=" width: 100%;" id ="livefeed">	
			  
			
			</div>
			
		</div>
		<br>
      	<br>
       
       
    <!-- End Right Column -->
    </div>
    
    <!-- guide1 modal -->
    <div id="guideDiv" class="ui longer modal">
	  <i class="close icon"></i>
	  <div class="header">
	    <i class="question circle icon"></i> KIOKI 紹介
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide1.PNG">
	    </div>
	    <div class="description">
	      <div class="ui header">
	      	ウェブとモバイルを活用して地域住民とユーザに各地域の天気、生活安全関連の位置情報を迅速に提供するウェブサイト。
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- guide2 modal -->
    <div id="guide2Div" class="ui longer modal">
	  <i class="close icon"></i>
	  <div class="header">
	    <i class="question circle icon"></i> メインガイド
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide2.PNG">
	    </div>
	    <div class="description">
	      <div class="ui header">
	      	日本全国の状況を見ることができます。<br>
	      	地震、台風、事件事故のようなキーワードでツイッターを確認することができます。
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- guide3 modal -->
    <div id="guide3Div" class="ui longer modal">
	  <i class="close icon"></i>
	  <div class="header">
	    <i class="question circle icon"></i> 地域ページ
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide3.PNG">
	    </div>
	    <div class="description">
	      <div class="ui header">
	      	한 지역의 상세하게 볼 수 있습니다
	      </div>
	    </div>
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide6.PNG">
	    </div>
	    <div class="description">
	    	<div class="ui large image">
	      		<img src="${pageContext.request.contextPath}/resources/img/guide/guide10.png" >
	      		<div class="ui header">
	      			점수 소개
	      		</div>
	      	</div>
	    </div>
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide4.PNG">
	    </div>
	    <div class="description">
	      <div class="ui header">
	      	지도 기능 소개
	      </div>
	    </div>
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide5.PNG">
	    </div>
	    <div class="description">
	      <div class="ui header">
	        	시설물 소개
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- guide4 modal -->
    <div id="guide4Div" class="ui longer modal">
	  <i class="close icon"></i>
	  <div class="header">
	    <i class="question circle icon"></i> ライブフィード
	  </div>
	  <div class="image scrolling content">
	    <div class="ui medium image">
	      <img src="${pageContext.request.contextPath}/resources/img/guide/guide12.PNG">
	    </div>
	    <div class="description">
	      <div class="ui header">
	      	라이브피드 소개
	      </div>
	    </div>
	  </div>
	</div>
	
  <!-- End Grid -->
  </div>
  <input type="hidden" id="lng" value="${sessionScope.loginlocal.getLongitude()}">
  <input type="hidden" id="lat" value="${sessionScope.loginlocal.getLatitude()}">
<%@ include file="inc/footer.jsp" %>

