/**
 * kioki JS
 * 공통으로 쓰이면 유용한 함수를 정의합니다.
 */
//geoCode는 주소로 경위도를 찾는다.
function geoCode(todofuken,shiku,callbackContext,callback){
	//todofuken: 도도부현
	//shiku: 시구 (optional)
	//callbackContext: this로 넘겨질 object reference
	//callback: 콜백 함수
	
	/*지오코딩 요청의 필수 매개변수:
	*
    * address — 지오코딩하려는 거리 주소. 이 주소는 해당 국가의 국내 우편 서비스에서 사용되는 형식입니다. 사업체 이름과 부서, 스위트 번호 또는 층 번호와 같은 추가적인 주소 요소는 피해야 합니다. 추가적인 지침은 FAQ를 참조하세요.
         또는
    * components — 지오코드를 구하려는 구성 요소 필터. 자세한 내용은 구성 요소 필터링을 참조하세요. address가 제공되는 경우, 구성 요소 필터는 또한 선택적 매개변수로 허용됩니다.
    * key — 애플리케이션의 API 키. 이 키는 할당량 관리를 위해 애플리케이션을 식별합니다. 키 가져오기 방법에 대해 알아봅니다.
    * 참고: Google Maps API 프리미엄 플랜 고객은 Geocoding 요청에서 API 키를 사용하거나 유효한 클라이언트 ID와 디지털 서명을 사용할 수 있습니다. 자세한 내용은 프리미엄 플랜 고객의 인증 매개변수를 참조하세요.
    */
	$.ajax({
		url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+todofuken+(typeof shiku == "string"?' '+shiku:'')+'&key=AIzaSyClDCEjwzxMODU_oPzJwX3g0554e-SXB0A'
		,type: 'jsonp'
		,method: 'get'
		,success: function(data){
			callback.call(callbackContext,data);
		},error: function(obj,err){
			console.log("ajax geoCode error: "+err);
		}
	});	
}

//codeGeo는 경위도로 주소를 찾는다. 
function codeGeo(lat,long,callbackContext,callback){
	//lat: 위도
	//long: 경도
	//callbackContext: 콜백 함수를 위한 this reference
	//callback: 콜백 함수
	/*
	 * 지오코딩 요청의 필수 매개변수:
	 *
	 *   address — 지오코딩하려는 거리 주소. 이 주소는 해당 국가의 국내 우편 서비스에서 사용되는 형식입니다. 사업체 이름과 부서, 스위트 번호 또는 층 번호와 같은 추가적인 주소 요소는 피해야 합니다. 추가적인 지침은 FAQ를 참조하세요.
	 *        또는
	 *   components — 지오코드를 구하려는 구성 요소 필터. 자세한 내용은 구성 요소 필터링을 참조하세요. address가 제공되는 경우, 구성 요소 필터는 또한 선택적 매개변수로 허용됩니다.
	 *   key — 애플리케이션의 API 키. 이 키는 할당량 관리를 위해 애플리케이션을 식별합니다. 키 가져오기 방법에 대해 알아봅니다.
	 *
	 *   참고: Google Maps API 프리미엄 플랜 고객은 Geocoding 요청에서 API 키를 사용하거나 유효한 클라이언트 ID와 디지털 서명을 사용할 수 있습니다. 자세한 내용은 프리미엄 플랜 고객의 인증 매개변수를 참조하세요.
     *
	 */
	$.ajax({
		url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+long+'&language=ja&result_type=locality&key=AIzaSyClDCEjwzxMODU_oPzJwX3g0554e-SXB0A'
		,type: 'jsonp'
		,method: 'get'
		,success: function(data){
			callback.call(callbackContext,data);
		},error: function(obj,err){
			console.log("ajax codeGeo error: "+err);
		}
	});
}

function getAlert(path){
	$.ajax({
		url:path+'localView/selectAlert'
		,type: 'json'
		,method: 'get'
		,success: function(data){
			if(data != null && data.length > 0){
				var msgbody = '<div class="ui accordion">';
				data.forEach(function(each){
					msgbody+='<button class="ui grey basic mini button title" style="margin-top:5px;">'+each["title"]+'▼</button><div class="content">'+each["content"]+"</div>";
				});
				msgbody+="</div>";
				$('#alertbox p').html(msgbody);
				
				$('#alertbox').show();
				$('.ui.accordion')
				  .accordion();
			}else{
				$('#alertbox p').html('');
				$('#alertbox').hide();
			}
		},error: function(obj,err){
			console.log("ajax alert error: "+err);
		}
	});
}